﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for Notify8
/// </summary>
namespace Notify8.Helper
{
    public static class Notify
    {
        public enum NotificationType{

            info, warning, error, success
        }
        public static string NotificationMessage(string title,string message, NotificationType type)
        {
            StringBuilder build = new StringBuilder();
            build.AppendFormat(@"<script type=""text/javascript"">");
            build.AppendFormat("$.notific8('{0}',{{heading: '{1}' ,theme: '{2}'}});", message, title, type);
            build.AppendFormat("</script>");
            return build.ToString();
        }

        public static void ShowNotification(this Page page, string tile, string message, NotificationType mytype)
        {
            page.ClientScript.RegisterStartupScript(page.GetType(), "Info", Notify.NotificationMessage(tile, message,mytype));

        }
    }
}