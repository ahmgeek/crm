﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Stuff.aspx.cs" Inherits="SystemSettings_Stuff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div id="content">
        <div class="outer">
            <div class="inner">

    <div class="box">
       <header>
           <div class="icons"><i class="icon-user"></i></div>
           <h5>
               <span class="label label-danger">Profile</span>
           </h5>
           <div class="toolbar">
               <div class="btn-group">
                   <a href="#div-1" data-toggle="collapse" class="btn btn-default btn-sm accordion-toggle minimize-box">
                       <i class="icon-chevron-up"></i>
                   </a>
                   <a class="btn btn-danger btn-sm close-box"><i class="icon-remove"></i></a>
               </div>
           </div>
       </header>

        <div id="div-1" class="accordion-body collapse in body">
            <div id="form" class="form-horizontal">
                <div class="form-group">
                    <label for="text1" class="col-lg-2">First Name</label>

                    <div class="col-lg-3">

                        <asp:TextBox ID="txtFName" runat="server" class="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ErrorMessage="*" ForeColor="Red" ControlToValidate="txtFName" runat="server" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="text1" class="col-lg-2">Surr name</label>

                <div class="col-lg-3">

                    <asp:TextBox ID="txtSurrName" runat="server" class="form-control input-tooltip"
                        data-original-title="Surr name"
                        data-placement="right"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="*" ForeColor="Red" ControlToValidate="txtSurrName" runat="server" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2"> Date Of Birth  </label>
                <div class="col-lg-3">
                       <div class="input-group input-append  date" id="dpYears" data-date="" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                                    <asp:TextBox
                                                        ID="txtDateOf" runat="server" type="text"  placeholder="00/00/0000"
                                                        CssClass="form-control" data-date-format="dd/mm/yyyy" required></asp:TextBox>
                                                    <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                                                </div>

                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2">Moblie</label>

                <div class="col-lg-3">
                    <asp:TextBox ID="txtMob" runat="server" class="form-control">
                    </asp:TextBox>
                   

                </div>
            </div>

        </div>

        <div class="form-actions">
         <asp:Button ID="btnInsert" Visible="false" type="submit" Width="70" Height="35"  class="btn btn-success btn-round" runat="server" Text="Insert" OnClick="btnInsert_Click" />

            <asp:Button ID="btnSave" Visible="false" type="submit" Width="70" Height="35"  class="btn btn-success btn-round" runat="server" Text="Save" OnClick="btnSave_Click" />

            <asp:LinkButton ID="lnk" class="btn btn-danger btn-round"
                CausesValidation="false" Width="70" Height="35"  PostBackUrl="/SystemSettings/Users.aspx" runat="server">Cancel</asp:LinkButton>
         
             </div>

    </div>
        </div>
                    </div>
        </div>                    </div>


</asp:Content>

