﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;

public partial class SystemSettings_NewUser : System.Web.UI.Page
{
    RolesBLL rolesBLL = new RolesBLL();
    UsersBLL userBll = new UsersBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtPass.Text = "";
            txtMail.Text = "";
            bindChk();
        }
    }



    protected void btnSave_Click(object sender, EventArgs e)
    {
        string[] arr;
        ArrayList roleList = new ArrayList();
        foreach (ListItem item in chkList.Items)
        {
            if (item.Selected)
            {
                roleList.Add(item.Text);
            }
        }

        arr = (string[])roleList.ToArray(typeof(string));
        try
        {
            userBll.CreatUser(txtUserName.Text, txtMail.Text, txtPass.Text, arr);
            Response.Redirect("/SystemSettings/Users.aspx", false);
        }
        catch (Exception)
        {
            Response.Redirect("/SystemSettings/Users.aspx", false);
        }

    }


    void bindChk()
    {
        chkList.DataSource = rolesBLL.AllRoles();
        chkList.DataBind();
    }
}