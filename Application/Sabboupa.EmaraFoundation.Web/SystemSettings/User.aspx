﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="User.aspx.cs" Inherits="SystemSettings_NewUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
           <div id="content">
        <div class="outer">
            <div class="inner">

    <!--BEGIN INPUT TEXT FIELDS-->
    <div class="row-fluid">
        <div class="span12">


            <div class="box inverse">
                <header>
                    <div class="icons"><i class="icon-male"></i></div>
                    <h5>New User</h5>
                    <!-- .toolbar -->
                    <div class="toolbar">
                        <ul class="nav">
                            <li>
                                <a href="/SystemSettings/Users.aspx">
                                    <i class="icon-arrow-left"></i>Back
                                </a>

                            </li>
                            <li>
                                <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#divData">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.toolbar -->
                </header>
                <div id="divData" class="accordion-body collapse in body">
                    <div id="form" class="form-horizontal">

                        <div class="form-group">
                            <label class="col-lg-2">User Name</label>

                            <div class="col-lg-3">

                                <asp:TextBox ID="txtUserName" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-lg-2">Email</label>

                            <div class="col-lg-3">
                                <asp:TextBox ID="txtMail" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2">Password</label>

                            <div class="col-lg-3">
                                <asp:TextBox ID="txtPass" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2">Re Password</label>

                            <div class="col-lg-3">
                                <asp:TextBox ID="txtRePassword" runat="server" class="form-control"
                                    TextMode="Password"></asp:TextBox>
                                <asp:CompareValidator
                                    ErrorMessage="Passwords Not Matching ! "
                                    ControlToValidate="txtPass" ControlToCompare="txtRePassword" runat="server" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2">Role </label>
                            <div class="col-lg-3">
                                <asp:CheckBoxList ID="chkList"
                                    RepeatColumns="1"
                                    CssClass="checkbox"
                                    runat="server">
                                </asp:CheckBoxList>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <asp:Button ID="btnSave" type="submit" class="btn btn-primary" runat="server" Text="Save" OnClick="btnSave_Click" />


                    </div>

                </div>
            </div>
        </div>
    </div>
  </div>
        </div>
    </div>
</asp:Content>

