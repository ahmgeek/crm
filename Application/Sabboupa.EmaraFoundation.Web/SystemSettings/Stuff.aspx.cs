﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;
using Notify8.Helper;

public partial class SystemSettings_Stuff : System.Web.UI.Page
{


    private Guid stuffId = Guid.Empty;
    private string stuffName;
    UsersBLL u = new UsersBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString.ToString() != string.Empty)
        {
            // getting the Id of the user
            stuffId = (Guid)u.GetUser(Request.QueryString["name"].ToString()).ProviderUserKey;
            stuffName = Request.QueryString["name"].ToString();
        }
        else
        {
            Response.Redirect("/Default.aspx");
        }

        if (!IsPostBack)
        {
            if (stuffId != Guid.Empty )
            {
                try
                {
                    Data(stuffId);
                    btnSave.Visible = true;
                    btnInsert.Visible = false;
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    this.ShowNotification("Warning", message, Notify.NotificationType.info);
                    btnInsert.Visible = true;
                    btnSave.Visible = false;

                }

            }





        }
    }


    private void Data(Guid id)
    {
        Stuff myStuff = StuffBLL.GetItem(stuffName);
        if (u.GetUser().ProviderUserKey.ToString() == id.ToString())
        {
            txtFName.Text = myStuff.FirstName;
            txtSurrName.Text = myStuff.SurrName;
            txtDateOf.Text = myStuff.DateOfBirth.ToShortDateString();
            txtMob.Text = myStuff.Mob;
        }
        else
        {
            this.ShowNotification("Error", "you are not approved to see this ! ", Notify.NotificationType.info);
        }
      
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Stuff stuff = new Stuff();
        if (u.GetUser().ProviderUserKey.ToString() == stuffId.ToString())
        {
            stuff.StuffId = stuffId;
            stuff.FirstName = txtFName.Text;
            stuff.SurrName = txtSurrName.Text;
            stuff.DateOfBirth = Convert.ToDateTime(txtDateOf.Text);
            stuff.Mob = txtMob.Text;
            StuffBLL.Update(stuff);
            this.ShowNotification("Updated", "Data Has Been Updated", Notify.NotificationType.success);
        }
        else
        {
            this.ShowNotification("Error", "you are not approved to see this ! ", Notify.NotificationType.info);

        }
    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        Stuff stuff = new Stuff();
        if (u.GetUser().ProviderUserKey.ToString() == stuffId.ToString())
        {
            stuff.StuffId = stuffId;
            stuff.FirstName = txtFName.Text;
            stuff.SurrName = txtSurrName.Text;
            stuff.DateOfBirth = Convert.ToDateTime(txtDateOf.Text);
            stuff.Mob = txtMob.Text;
            StuffBLL.Insert(stuff);
            this.ShowNotification("Saved", "Data Has Been Saved", Notify.NotificationType.success);
        }
        else
        {
            this.ShowNotification("Error", "you are not approved to see this ! ", Notify.NotificationType.info);
            
        }
    }
}