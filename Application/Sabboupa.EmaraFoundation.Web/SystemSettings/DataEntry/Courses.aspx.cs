﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Panda.EmaraSystem.BO;
using Panda.EmaraSystem.BLL;

public partial class SystemSettings_DataEntry_Courses : System.Web.UI.Page
{
    private int rankUser = 0;
    private int currentPageUser = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }

    }

    private void BindGrid()
    {
        try
        {
            List<PrescriptionCourseData> ds = PrescriptionCourseDataBLL.GetList();
            grd.DataSource = ds; //clntBLL.GetAllClients();
            grd.DataBind();

        }
        catch (Exception ex)
        {

            grd.EmptyDataText = ex.Message;
            grd.DataBind();
        }

    }

    protected void btnSave_OnClick(object sender, EventArgs e)
    {
        PrescriptionCourseData prsqCourseData = new PrescriptionCourseData();
        prsqCourseData.CourseDataName = txtData.Text;
        PrescriptionCourseDataBLL.Insert(prsqCourseData);
        txtData.Text = "";
        BindGrid();
        
    }

    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        PrescriptionCourseData prsqCourseData = new PrescriptionCourseData();
        prsqCourseData.CourseDataId = Convert.ToInt16(Application["id"]);
        prsqCourseData.CourseDataName = txtData.Text;
        PrescriptionCourseDataBLL.update(prsqCourseData);
        txtData.Text = "";
        btnSave.Visible = true;
        btnUpdate.Visible = false;
        btnCancel.Visible = false;

        BindGrid();
    }

    protected void grd_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
                Label lblRank = (Label)e.Row.FindControl("lblRank");
        // On page reload, rank is reset to 0
        if (rankUser == 0)
        {
            // Only run this on subsequent pages
            if (currentPageUser > 0)
            {
                // Set rank to current index of page * the number of records to display on GridView page
                rankUser = currentPageUser * grd.PageSize;
            }
        }
        // Make sure we actually found our label
        if (lblRank != null)
        {
            // Increment rank by 1
            rankUser += 1;
            // Set rank label to our new rank value
            lblRank.Text = rankUser.ToString();
        }
    }

    protected void grd_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        currentPageUser = e.NewPageIndex;
        grd.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void grd_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        int id;
        if (e.CommandName == "EditCommand")
        {
            id = Convert.ToInt32(e.CommandArgument);
            PrescriptionCourseData prsqCourseData = PrescriptionCourseDataBLL.GetItem(id);
            txtData.Text = prsqCourseData.CourseDataName;
            btnSave.Visible = false;
            btnUpdate.Visible = true;
            btnCancel.Visible = true;

            Application["id"] = id;
        }
        else if (e.CommandName == "DeleteCommand")
        {
            id = Convert.ToInt32(e.CommandArgument);
            PrescriptionCourseData prsqCourseData = PrescriptionCourseDataBLL.GetItem(id);
            PrescriptionCourseDataBLL.Delete(prsqCourseData);
            BindGrid();

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/SystemSettings/DataEntry/Courses.aspx");

    }
}