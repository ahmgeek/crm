﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManageData.aspx.cs" Inherits="SystemSettings_DataEntry_ManageData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <div class="outer">
            <div class="inner">

                <a class="quick-btn" style="width: 120px;" href="/SystemSettings/DataEntry/SessionQuestions.aspx">
                    <i class="icon-question-sign icon-large "></i>
                    <span>Client Questions</span>
                    <span class="label label-important"></span>
                </a>
                <a class="quick-btn" style="width: 120px;" href="/SystemSettings/DataEntry/Cds.aspx">
                    <i class="icon-play-sign icon-large "></i>
                    <span>CD's</span>
                    <span class="label label-important"></span>
                </a>
                <a class="quick-btn" style="width: 120px;" href="/SystemSettings/DataEntry/Courses.aspx">
                    <i class="icon-bullhorn icon-large "></i>
                    <span>Courses</span>
                    <span class="label label-important"></span>
                </a>
                <a class="quick-btn" style="width: 120px;" href="/SystemSettings/DataEntry/Sessions.aspx">
                    <i class="icon-search icon-large "></i>
                    <span>Sessions</span>
                    <span class="label label-important"></span>
                </a>
            </div>
        </div>
    </div>
</asp:Content>

