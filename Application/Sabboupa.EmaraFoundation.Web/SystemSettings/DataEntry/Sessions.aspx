﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Sessions.aspx.cs" Inherits="SystemSettings_DataEntry_Sessions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       
     <div id="content">
        <div class="outer">
            <div class="inner">
    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            

    <div class="box inverse">
        <header>
            <div class="icons"><i class="icon-question"></i></div>
            <h5>Session Questions</h5>
          <div class="toolbar">
                        <ul class="nav">
                             <li>
                        <a class="btn btn-link" href="/SystemSettings/DataEntry/ManageData.aspx"><i class="icon-arrow-left"></i>Back
                        </a>
                            <li>
                                <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#UserTable">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
        </header>
        <div id="UserTable" class="body collapse in">
      <div id="id" class="form-horizontal controls-row">

               <div class="form-group">
                                <label for="text1" class="col-lg-2">Session Name : </label>

                                <div class="col-lg-4">

                                    <asp:TextBox ID="txtData"  runat="server" class="form-control" required></asp:TextBox>
                                     </div> <div class="col-lg-4">
                                    <asp:Button runat="server" ID="btnSave" Text="Save" 
                                       OnClick="btnSave_OnClick" CssClass="btn btn-primary"/>
                                      <asp:Button runat="server" ID="btnUpdate" Text="Update" 
                                    Visible="False"   OnClick="btnUpdate_OnClick" CssClass="btn btn-success"/>
                                            <asp:Button runat="server" ID="btnCancel" Text="Cancel" 
                                    Visible="False" OnClick="btnCancel_Click"  CssClass="btn btn-default"/>
                                </div>
                            </div>
          </div>
             <div class="form-horizontal controls-row" dir="rtl">
                 
                <asp:GridView ID="grd" 
                CssClass="table  table-bordered table-condensed table-hover table-striped"
                runat="server"
                GridLines="None"
                CellSpacing="-1"
                AutoGenerateColumns="False"
                ShowFooter="True" ShowHeaderWhenEmpty="True" 
                EmptyDataText="Empty !" 
                OnRowDataBound="grd_OnRowDataBound"
                OnPageIndexChanging="grd_OnPageIndexChanging"
                AllowPaging="True" OnRowCommand="grd_OnRowCommand" PageSize="25">
                <Columns>
                <asp:TemplateField HeaderText="#">
                <ItemTemplate>
                    <asp:Label ID="lblRank" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>

                    <asp:BoundField DataField="SessionDataName"  HeaderText="Sessions" />
                    <asp:TemplateField HeaderText="Action">
                <ItemTemplate>

                           <asp:LinkButton ID="btnEdit" runat="server" 
                               CssClass="btn btn-small btn-metis-5" Text="Edit"
                               CommandArgument='<%# Eval("SessionDataID") %>'  CommandName="EditCommand" />
                            
                             <asp:LinkButton ID="LinkButton2"
                                 runat="server" CssClass="btn btn-small btn-metis-1" Text="Delete"  OnClientClick="return confirm('Are You Sure?')"
                            CommandArgument='<%# Eval("SessionDataID") %>' CommandName="DeleteCommand" />
                </ItemTemplate>

                    </asp:TemplateField>

                </Columns>

            </asp:GridView>

                 </div>

        </div>
    </div>
            
        </ContentTemplate>
    </asp:UpdatePanel>
                   </div>

        </div>
    </div>
</asp:Content>

