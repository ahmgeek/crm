﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Notify8.Helper;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;
public partial class Clients_ClientProfile : System.Web.UI.Page
{

    UsersBLL user = new UsersBLL();
    int clientID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Request.QueryString.ToString() != string.Empty)
            {
                clientID = Convert.ToInt32(Request.QueryString["id"]);
                BindData();
            }
            else
            {
                Response.Redirect("/Clients/Clients.aspx");
            }
        }
    }
    private void BindData()
    {
        try
        {
            Client client = ClientBLL.GetItem(Convert.ToInt32(Request.QueryString["id"]));
            int id = Convert.ToInt32(Request.QueryString["id"]);
            lblName.Text = client.FullName;
            lblCity.Text = client.City;
            lblCountry.Text = client.Country;
            lblAddress.Text = client.Address;
            lblTelephone.Text = client.Telephone;
            lblMob.Text = client.Mob;
            DateTime today = DateTime.Today;
            int age = today.Year - client.DateOfBirth.Year;
            lblAge.Text = age.ToString();
            lblGender.Text = client.Gender;
            lblNotes.Text = client.Notes;
        }
        catch (Exception ex)
        {
            this.ShowNotification("Error", ex.Message, Notify.NotificationType.info);
        }
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Clients/ClientsDetail.aspx?id=" + Convert.ToInt32(Request.QueryString["id"]));
    }
}