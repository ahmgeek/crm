﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientsDetail.aspx.cs" Inherits="Clients_ClientsDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <div class="outer">
            <div class="inner">
                
                
                <div class="row">
                    <div class="col-lg-7">
                        
                  
                <div class="box inverse">

                    <header>
                        <div class="icons"><i class="icon-male"></i></div>
                        <h5>Client Information</h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                            <ul class="nav">
                                <li>
                                    <a href="/Clients/Clients.aspx">
                                        <i class="icon-arrow-left"></i>Back
                                    </a>

                                </li>

                            </ul>
                        </div>
                        <!-- /.toolbar -->
                    </header>
                      <div id="divData" class="accordion-body collapse in  body">
                    <div class="form-horizontal">

                

                        <div class="form-group">
                            <label  class="col-lg-3">First Name</label>

                            <div class="col-lg-4">

                                <asp:TextBox ID="txtFName" CssClass="form-control"
                                    placeholder="First Name" runat="server" required></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3">Middle Name</label>

                            <div class="col-lg-4">

                                <asp:TextBox ID="txtMiddleName" CssClass="form-control"
                                    placeholder="MiddleName" 
                                    runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3">SurrName</label>

                            <div class="col-lg-4">

                                <asp:TextBox ID="txtSurrName" CssClass="form-control"
                                    placeholder="Surr Name"
                                    runat="server" required></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3">City</label>

                            <div class="col-lg-4">

                                <asp:TextBox ID="txtCity" CssClass="form-control"
                                    placeholder="City"
                                    runat="server" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3">Country</label>

                            <div class="col-lg-4">
                                <asp:DropDownList CssClass="form-control chzn-select" runat="server" ID="drpCountry">
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3">Address</label>

                            <div class="col-lg-4">

                                <asp:TextBox ID="txtAdress" TextMode="MultiLine" CssClass="form-control"
                                    placeholder="Please enter the Address"
                                    runat="server"></asp:TextBox>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="text2" class="col-lg-3">Telephone</label>

                            <div class="col-lg-4">

                                <asp:TextBox ID="txtTelephone" CssClass="form-control"
                                    placeholder="Please entre Phone Number"
                                    runat="server" pattern="\d+"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="limiter" class="col-lg-3">Mobile</label>

                            <div class="col-lg-4">

                                <asp:TextBox ID="txtMob" CssClass="form-control"
                                    placeholder="Please enter Mobile Number"
                                    runat="server" required pattern="\d+"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="text4" class="col-lg-3">Date Of Birth</label>

                            <div class="col-lg-3">

                                <div class="input-group input-append  date" id="dpYears" data-date="" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                    <asp:TextBox
                                        ID="txtDateOf" runat="server" type="text" placeholder="00/00/0000"
                                        CssClass="form-control" data-date-format="dd/mm/yyyy" required></asp:TextBox>
                                    <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                                </div>

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3">Gender</label>


                            <div class="col-lg-4">

                                <asp:DropDownList ID="drpGender" CssClass="form-control chzn-select" runat="server">
                                    <asp:ListItem></asp:ListItem>
                                    <asp:ListItem>Male</asp:ListItem>
                                    <asp:ListItem>Female</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3">Notes</label>

                            <div class="col-lg-4">

                                <asp:TextBox ID="txtNotes" TextMode="MultiLine" CssClass="form-control"
                                    placeholder="Notes "
                                    runat="server"></asp:TextBox>
                            </div>


                        </div>

                        <hr />

                        <asp:Panel ID="pnlRelation" Visible="true" runat="server">

                            <div class="form-group">
                                <label  class="col-lg-3">Has a Relation ? </label>

                                <div class="col-lg-4">
                                    <asp:RadioButtonList ID="rdlst" runat="server"
                                        RepeatDirection="Horizontal" CellPadding="9" AutoPostBack="true" OnSelectedIndexChanged="rdlst_SelectedIndexChanged">
                                        <asp:ListItem Value="1"> Yes </asp:ListItem>
                                        <asp:ListItem Value="0"> No </asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator runat="server" ID="req"
                                        ControlToValidate="rdlst" ForeColor="red" ErrorMessage="You must specify the relation"></asp:RequiredFieldValidator>

                                </div>

                            </div>
                            <asp:Panel runat="server" ID="pnlRelationData">
                                <div class="form-group">
                                    <label class="col-lg-3">Related To </label>
                                    <div class="col-lg-4" style="height: 150px;">
                                        <asp:DropDownList ID="drpClients" runat="server"
                                            CssClass="form-control chzn-select">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3">Relation Name</label>

                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtRelName" runat="server" placeholder="Enter the relation name" required CssClass="form-control">
                                        </asp:TextBox>
                                    </div>
                                </div>

                            </asp:Panel>

                        </asp:Panel>



                    </div>
                    <div class="form-group">

                        <div class="form-actions no-margin-bottom">
                            <asp:Button ID="btnSave" Text="Update"
                                runat="server" OnClick="btnSave_Click" class="btn btn-primary" />
                            &nbsp;
                                        <asp:Button ID="btnCanel" Text="Cancel"
                                            runat="server" OnClick="btnCanel_OnClick"  CausesValidation="false" class="btn btn-warning" />

                        </div>

                    </div>
                </div>
                  </div>
                </div>
            </div>


        </div>

    </div>
    
    </div>


</asp:Content>

