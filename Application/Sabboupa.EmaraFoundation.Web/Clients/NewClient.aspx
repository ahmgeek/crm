﻿<%@ Page Title="New Client" Language="C#" MasterPageFile="~/MasterPage.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="NewClient.aspx.cs" Inherits="Clients_NewClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="content">
      
        <div class="outer">
            <div class="inner">
          <div class="row">

                        <div class="col-lg-7">


                            <div class="box inverse">
                                <header>
                                    <div class="icons"><i class="icon-male"></i></div>
                                    <h5>Client Information</h5>
                                    <!-- .toolbar -->
                                    <div class="toolbar">
                                        <ul class="nav">
                                            <li>
                                                <a href="/Clients/Clients.aspx">
                                                    <i class="icon-arrow-left"></i>Back
                                                </a>

                                            </li>
                                            <li>
                                                <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#divData">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.toolbar -->
                                </header>
                                <div id="divData" class="accordion-body collapse in  body">

                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label for="text1" class="col-lg-3">Case Number</label>

                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txtCaseNumber" ReadOnly="true" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="pass1" class="col-lg-3">First Name</label>

                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txtFName" CssClass="form-control"
                                                    placeholder="First Name" runat="server" required></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3">Middle Name</label>

                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txtMiddleName" CssClass="form-control"
                                                    placeholder="MiddleName"
                                                    runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3">SurrName</label>

                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txtSurrName" CssClass="form-control"
                                                    placeholder="Surr Name"
                                                    runat="server" required></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3">City</label>

                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txtCity" CssClass="form-control"
                                                    placeholder="City"
                                                    runat="server" required></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3">Country</label>

                                            <div class="col-lg-4">
                                                <asp:DropDownList CssClass="form-control chzn-select" runat="server" ID="drpCountry">
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3">Address</label>

                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txtAdress" TextMode="MultiLine" CssClass="form-control"
                                                    placeholder="Please enter the Address"
                                                    runat="server"></asp:TextBox>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="text2" class="col-lg-3">Telephone</label>

                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txtTelephone" CssClass="form-control"
                                                    placeholder="Please entre Phone Number"
                                                    runat="server" pattern="\d+"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="limiter" class="col-lg-3">Mobile</label>

                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txtMob" CssClass="form-control"
                                                    placeholder="Please enter Mobile Number"
                                                    runat="server" required pattern="\d+"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="text4" class="col-lg-3">Date Of Birth</label>

                                            <div class="col-lg-4">

                                                <div class="input-group input-append  date" id="dpYears" data-date="01-01-1990" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                                    <asp:TextBox
                                                        ID="txtDateOf" runat="server" type="text" placeholder="00/00/0000"
                                                        CssClass="form-control" data-date-format="dd-mm-yyyy" required></asp:TextBox>
                                                    <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="autosize" class="col-lg-3">Gender</label>


                                            <div class="col-lg-4">

                                                <asp:DropDownList ID="drpGender" CssClass="form-control chzn-select" runat="server">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem>Male</asp:ListItem>
                                                    <asp:ListItem>Female</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="tags" class="col-lg-3">Notes</label>

                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txtNotes" TextMode="MultiLine" CssClass="form-control"
                                                    placeholder="Notes "
                                                    runat="server"></asp:TextBox>
                                            </div>


                                        </div>


                                        <div class="form-group">
                                            <label for="pass1" class="col-lg-3">Has a Relation ? </label>

                                            <div class="col-lg-4">
                                                <asp:RadioButtonList ID="rdlst" runat="server"
                                                    RepeatDirection="Horizontal" CellPadding="9" AutoPostBack="true" OnSelectedIndexChanged="rdlst_SelectedIndexChanged">
                                                    <asp:ListItem Value="1"> Yes </asp:ListItem>
                                                    <asp:ListItem Value="0"> No </asp:ListItem>
                                                </asp:RadioButtonList>
                                                <asp:RequiredFieldValidator runat="server" ID="req"
                                                    ControlToValidate="rdlst" ForeColor="red" ErrorMessage="You must specify the relation"></asp:RequiredFieldValidator>

                                            </div>

                                        </div>



                                        <asp:Panel ID="pnlRelation" Visible="False" runat="server">
                                            <div class="form-group">
                                                <label class="col-lg-3">Related To </label>

                                                <div class="col-lg-4">
                                                    <asp:DropDownList ID="drpClients" runat="server"
                                                        CssClass="form-control chzn-select">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-lg-3">Relation Name</label>

                                                <div class="col-lg-4">
                                                    <asp:TextBox ID="txtRelName" runat="server" placeholder="Enter the relation name" required CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                    </div>
                                </div>
                            </div>
                        </div>

                </div>

                 <div class="row">

                        <div class="col-lg-7">
                            <div class="box inverse">

                                <header>
                                    <div class="icons"><i class="icon-question-sign "></i></div>
                                    <h5>Question</h5>
                                    <div class="toolbar">
                                        <ul class="nav">
                                            <li>
                                                <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#divPresc">
                                                    <i class="icon-chevron-down"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </header>

                                <div id="divPresc" class="accordion-body collapse in body">

                                    <div class="form-horizontal">

                                        <asp:Repeater ID="questionRepeater" ViewStateMode="Enabled" runat="server">
                                            <ItemTemplate>
                                                <div class="form-group">
                                                    <label class="col-lg-2">Question  : </label>
                                                    <div class="col-lg-9" dir="rtl">
                                                        <asp:Label ID="lblQuestion" Style="float: left;" runat="server" Height="20" CssClass="control-label" Text='<%#Eval("SessionQuestion") %>'></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2">Answer : </label>
                                                    <div class="col-lg-5">
                                                        <asp:TextBox runat="server" ID="txtAns"
                                                            TextMode="MultiLine" Height="20%"
                                                            CssClass="form-control"></asp:TextBox>

                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-2">Final Report : </label>

                                            <div class="col-lg-5">
                                                <asp:TextBox runat="server" ID="txtReport"  required Height="150"  TextMode="MultiLine" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                     </div>


                   <div class="row">
                        <div class="col-lg-7">
                            <div class="box inverse">

                                <header>
                                    <div class="icons"><i class="icon-medkit"></i></div>
                                    <h5>Prescription</h5>
                                    <div class="toolbar">
                                        <ul class="nav">
                                            <li>
                                                <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#divSession">
                                                    <i class="icon-chevron-down"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </header>


                                <div id="divSession" class="accordion-body collapse in  body">
                                    <div id="Presc" class="form-horizontal">

                                        <div class="form-group">
                                            <label class="col-lg-2">CD's : </label>
                                            <div class="col-lg-4">
                                                <asp:ListBox ID="lstCD"
                                                    multiple CssClass="form-control  chzn-select chzn-rtl"
                                                    DataValueField="CdDataID"
                                                    DataTextField="CdDataName"
                                                    SelectionMode="Multiple"
                                                    runat="server"></asp:ListBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Courses : </label>

                                            <div class="col-lg-4">
                                                <asp:ListBox ID="lstCourses"
                                                    SelectionMode="Multiple"
                                                    DataValueField="CourseDataID"
                                                    DataTextField="CourseDataName"
                                                    multiple CssClass="form-control chzn-select  chzn-rtl"
                                                    runat="server"></asp:ListBox>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2">Sessions : </label>
                                            <div class="col-lg-7">
                                                  <div class="repeater-special" style="width: 145%;">
                                                <table class="table table-responsive  table-hover ">
                                                    <tbody>
                                                        <asp:Repeater ID="repeatSessions"  ViewStateMode="Enabled" OnPreRender="repeatSessions_OnPreRender" runat="server">
                                                            <ItemTemplate>

                                                                <tr>
                                                                    <td>
                                                                        <label>Counter</label>

                                                                        <asp:TextBox runat="server" ID="txtCounter"
                                                                            CssClass="form-control" Width="52">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <label>Session</label><br />
                                                                        <asp:TextBox runat="server" ID="txtSessionName" Width="126"
                                                                            Text='<%# Eval("SessionDataName")%>' ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <label>Comment</label><br />
                                                                        <asp:TextBox runat="server" ID="txtComment"  TextMode="MultiLine" Width="100"
                                                                            Height="50"
                                                                            CssClass="form-control">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <label>&nbsp;</label><br />
                                                                        <div class="make-switch">
                                                                            <asp:CheckBox ID="chkCourse" runat="server" />
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                            </div>

 </div>
                                        </div>

                                        <div class="form-group">

                                            <label class="col-lg-2">Final Report:</label>

                                            <div class="col-lg-8" style="padding-bottom: 50px;">
                                                <asp:TextBox ID="txtFinalReport" required Height="110" runat="server" CssClass="form-control"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                          
                                        </div>
                                          <div class="form-actions">
                                                
                                            <label class="col-lg-2"></label>
                                                <asp:Button ID="btnSendToConfirm" Width="300" Text="Save and send to Confirmation"
                                                    runat="server" CssClass="btn btn-success btn-large btn-round" OnClick="btnSendToConfirm_OnClick" />
                                            </div>


                                    </div>

                                </div>
                            </div>
                        </div>

                 </div>






            </div>
       

         </div>

    </div>

</asp:Content>

