﻿<%@ Page Title="Clients" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Clients.aspx.cs" Inherits="Clients_Clients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <div class="outer">
            <div class="inner">

                <a class="quick-btn" href="/Clients/NewClient.aspx">
                    <i class="icon-user icon-2x"></i>
                    <span>New Client</span>
                    <span class="label label-info">New Client</span>
                    
                </a>
                <div class="row">

                    <div class="col-lg-9">
               
                    <div class="box inverse">
                        <header>
                            <div class="icons"><i class="icon-user-md"></i></div>
                            <h5>
                                <span class="label label-danger">Clients</span>
                            </h5>
                                               
                            <div class="toolbar">
                                 <ul class="nav pull-right">
                                        <li>
                                            <asp:TextBox ID="txtSearch"   placeholder="Search" runat="server"></asp:TextBox>
                                        </li>
                                     <li>
                                         <asp:Button ID="btnSearch" runat="server" Height="24" CssClass="btn btn-default btn-sm btn-grad btn-rect" Text="Find" />
                                     </li>
                                     <li>
                                         <a href="#quick" data-toggle="collapse" class="accordion-toggle minimize-box">
                                             <i class="icon-chevron-up"></i>
                                         </a></li>


                                 </ul>
                            </div>
                        </header>

                        <div id="quick" class="accordion-body collapse in body">
                  

                            <div class="table-responsive table-special">

                            <asp:GridView ID="grdUsers"
                                CssClass="table table-bordered table-condensed table-hover table-striped"
                                runat="server"
                                GridLines="None"
                                CellSpacing="-1"
                                AutoGenerateColumns="False"
                                ShowFooter="True" ShowHeaderWhenEmpty="True"
                                EmptyDataText="Empty !"
                                OnRowDataBound="grdUsers_RowDataBound"
                                OnPageIndexChanging="grdUsers_PageIndexChanging"
                                OnPreRender="grdUsers_PreRender"
                                OnRowCommand="grdUsers_OnRowCommand"
                                AllowPaging="true"
                                PageSize="60">
                                <PagerStyle BackColor="#428bca"  ForeColor="#333333" Font-Size="15px" Font-Bold="true"   HorizontalAlign="Left" CssClass="pager" />
                                <PagerSettings   Mode="NumericFirstLast" />
                                <Columns>
                                    <asp:TemplateField HeaderText="#">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRank" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                            <a Font-Bold="true" style="text-align:left;" 
                                 class="label label-primary"
                                 href='<%# "/Clients/CLientProfile.aspx?id=" + Eval("ClientId") %>'
                                runat="server">
                                 <asp:Label ID="lblFullName" runat="server" Text='<%#Eval("FullName") %> '></asp:Label>  </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Regestration Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRegister" runat="server"  style="text-align:left;"  Text='<%#Eval("CreationDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="City">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCity" runat="server"  Text='<%#Eval("City") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Client Moblie">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMob" runat="server"  style="text-align:left;" Text='<%#Eval("Mob") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Has Relatives ? ">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRelation" runat="server"  style="text-align:left;"  Text='<%#Eval("HasArelation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEdit"
                                                CommandArgument='<%# Eval("ClientId") %>' CommandName="EditCommand"
                                                runat="server" CssClass="btn btn-primary btn-round btn-line" Text="Edit" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>
                            </div>
                        </div>
                    </div>
                        
                </div>
            </div>
            </div>
        </div>
    </div>

          <script type="text/javascript">
              $(document).ready(function () {

                  $('#<%=btnSearch.ClientID%>').click(function (e) {
                  $("#<%=grdUsers.ClientID%> tr:has(td)").hide(); // Hide all the rows.

            var iCounter = 0;
            var sSearchTerm = $('#<%=txtSearch.ClientID%>').val(); //Get the search box value

            if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
            {
                $("#<%=grdUsers.ClientID%> tr:has(td)").show();
                return false;
            }
            //Iterate through all the td.
            $("#<%=grdUsers.ClientID%> tr:has(td)").children().each(function () {
                var cellText = $(this).text().toLowerCase();
                if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
                {
                    $(this).parent().show();
                    iCounter++;
                    return true;
                }
            });
            if (iCounter == 0) {
            }
            e.preventDefault();
        })
          })
    </script>

</asp:Content>

