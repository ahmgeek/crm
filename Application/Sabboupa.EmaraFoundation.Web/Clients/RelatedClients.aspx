﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RelatedClients.aspx.cs" Inherits="Clients_RelatedClients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <div class="outer">
            <div class="inner">



                <div class="row">
                    <div class="col-lg-12">
                        <div class="box inverse">
                            <header>
                                <div class="icons"><i class="icon-user-md"></i></div>
                                <h5>
                                    <span class="label label-danger">Clients</span>
                                </h5>
                                <div class="toolbar">
                                    <div class="btn-group">
                                        <ul class="nav pull-right">
                                        <li>
                                            <asp:TextBox ID="txtSearch"   placeholder="Search" runat="server"></asp:TextBox>
                                        </li>
                                     <li>
                                         <asp:Button ID="btnSearch" runat="server" Height="24" CssClass="btn btn-default btn-sm btn-grad btn-rect" Text="Find" />
                                     </li>
                                            <li>
                                        <a href="#quick" data-toggle="collapse" class="accordion-toggle minimize-box">
                                            <i class="icon-chevron-up"></i>
                                        </a></li>
                                         </ul>
                                    </div>
                                </div>
                            </header>
                            <div id="quick" class="accordion-body collapse in body">
                                <asp:GridView ID="grdUsers"
                                    CssClass="table table-bordered responsive"
                                    runat="server"
                                    GridLines="None"
                                    CellSpacing="-1"
                                    AutoGenerateColumns="False"
                                    ShowFooter="True" ShowHeaderWhenEmpty="True"
                                    EmptyDataText="Empty !"
                                    OnRowDataBound="grdUsers_RowDataBound"
                                    OnPageIndexChanging="grdUsers_PageIndexChanging"
                                    OnPreRender="grdUsers_PreRender"
                                    OnRowCommand="grdUsers_OnRowCommand"
                                    AllowPaging="true"
                                    PageSize="10">
                                    <Columns>
                                        <asp:TemplateField HeaderText="#">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRank" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <a font-bold="true" style="text-align: left;"
                                                    class="label label-primary"
                                                    href='<%# "/Clients/CLientProfile.aspx?id=" + Eval("ClientId") %>'
                                                    runat="server">
                                                    <asp:Label ID="ClientName" runat="server" Text='<%#Eval("ClientName") %> '></asp:Label>
                                                </a>


                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Related To">
                                            <ItemTemplate>
                                                   <a font-bold="true" style="text-align: left;"
                                                    class="label label-success"
                                                    href='<%# "/Clients/CLientProfile.aspx?id=" + Eval("CLientRelId") %>'
                                                    runat="server">
                                                    <asp:Label ID="ClientRelName" runat="server" Text='<%#Eval("ClientRelName") %> '></asp:Label>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>




                                        <asp:TemplateField HeaderText="Relation">
                                            <ItemTemplate>
                                                <asp:Label ID="RelationName" runat="server" Style="text-align: left;" Text='<%#Eval("RelationName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>



                                    </Columns>

                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <script type="text/javascript">
          $(document).ready(function () {

              $('#<%=btnSearch.ClientID%>').click(function (e) {
                      $("#<%=grdUsers.ClientID%> tr:has(td)").hide(); // Hide all the rows.

                      var iCounter = 0;
                      var sSearchTerm = $('#<%=txtSearch.ClientID%>').val(); //Get the search box value

            if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
            {
                $("#<%=grdUsers.ClientID%> tr:has(td)").show();
                return false;
            }
                      //Iterate through all the td.
                      $("#<%=grdUsers.ClientID%> tr:has(td)").children().each(function () {
                          var cellText = $(this).text().toLowerCase();
                          if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
                          {
                              $(this).parent().show();
                              iCounter++;
                              return true;
                          }
                      });
                      if (iCounter == 0) {
                      }
                      e.preventDefault();
                  })
              })
    </script>
</asp:Content>

