﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;
using Notify8.Helper;
public partial class Clients_RelatedClients : System.Web.UI.Page
{
    private int rankUser = 0;
    private int currentPageUser = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString.ToString() != string.Empty)
        {
            string message = Request.QueryString["message"];
            this.ShowNotification("Info", message, Notify.NotificationType.info);
        }

        if (!IsPostBack)
        {
            BindGrid();

        }

    }



    protected void grdUsers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblRank = (Label)e.Row.FindControl("lblRank");
        // On page reload, rank is reset to 0
        if (rankUser == 0)
        {
            // Only run this on subsequent pages
            if (currentPageUser > 0)
            {
                // Set rank to current index of page * the number of records to display on GridView page
                rankUser = currentPageUser * grdUsers.PageSize;
            }
        }
        // Make sure we actually found our label
        if (lblRank != null)
        {
            // Increment rank by 1
            rankUser += 1;
            // Set rank label to our new rank value
            lblRank.Text = rankUser.ToString();
        }

    }
    protected void grdUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        currentPageUser = e.NewPageIndex;
        grdUsers.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void grdUsers_PreRender(object sender, EventArgs e)
    {

        foreach (GridViewRow item in grdUsers.Rows)
        {

            Label lblRank = (Label)item.FindControl("lblRank");

            for (int i = 0; i < rankUser; i++)
            {
                lblRank.CssClass = "badge";

            }


            Label ClientName = (Label)item.FindControl("ClientName");
            ClientName.Width = 200;

            Label ClientRelName = (Label)item.FindControl("ClientRelName");
            ClientRelName.Width = 200;



            Label RelationName = (Label)item.FindControl("RelationName");
            RelationName.CssClass = "label label-primary";
            RelationName.Width = 130;



        }
    }
    protected void grdUsers_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditCommand")
        {
            Response.Redirect("/Clients/ClientsDetail.aspx?id=" + e.CommandArgument);
        }
    }

    void BindGrid()
    {
        try
        {
            List<Relatives> ds = RelativesBLL.GetList();
            grdUsers.DataSource = ds;
            grdUsers.DataBind();

        }
        catch (Exception ex)
        {

            grdUsers.EmptyDataText = ex.Message;
            grdUsers.DataBind();
        }

    }
}