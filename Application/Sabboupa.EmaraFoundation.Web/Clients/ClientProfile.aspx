﻿<%@ Page Title="Profile" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientProfile.aspx.cs" Inherits="Clients_ClientProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <div class="outer">
            <div class="inner">
                <br />
                <div class="row">
                    <!-- .col-lg-6 -->
                    <div class="col-lg-6">

                          <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="label label-danger">Client Card</i>

                            </div>

                            <!-- /.panel-heading -->
                            <div class="panel-body"  style="overflow:scroll">
                                <img src="../assets/img/UserPic/client.png" data-bindattr-4="4" class="pull-left" />
                                <p>
                                </p>
                                <dl class="dl-horizontal pull-left">
                                    <dt>Full Name : </dt>
                                    <dd>
                                        <asp:Label ID="lblName" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>City : </dt>
                                    <dd>
                                        <asp:Label ID="lblCity" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Country : </dt>
                                    <dd>
                                        <asp:Label ID="lblCountry" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Address : </dt>
                                    <dd>
                                        <asp:Label ID="lblAddress" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Telephone : </dt>
                                    <dd>
                                        <asp:Label ID="lblTelephone" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Mobile : </dt>
                                    <dd>
                                        <asp:Label ID="lblMob" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Age : </dt>
                                    <dd>
                                        <asp:Label ID="lblAge" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Gender : </dt>
                                    <dd>
                                        <asp:Label ID="lblGender" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <br />
                                    <dt>Notes : </dt>
                                    <dd>
                                        <asp:TextBox ID="lblNotes" TextMode="MultiLine"
                                            ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                                    </dd>
                                </dl>

                            </div>
                            <!-- /.panel-body -->
                            <div class="panel-footer">
                                <asp:LinkButton ID="btnEdit"
                                    OnClick="btnEdit_Click" runat="server" CssClass="btn btn-primary btn-round btn-line" Text="Edit" />

                                <asp:LinkButton ID="btnClients"
                                    OnClick="btnEdit_Click" PostBackUrl="javascript: history.go(-1)" runat="server" CssClass="btn btn-primary btn-round btn-line" Text="Back" />
                            </div>

                        </div>
                        <!-- /.panel-footer -->
                    </div>
                          <div class="col-lg-6" >
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="label label-success">Client Case</i>

                            </div>

                            <!-- /.panel-heading -->
                            <div class="panel-body" style="overflow:scroll">
                                <img src="../assets/img/UserPic/client.png" data-bindattr-4="4" class="pull-left" />
                                <p>
                                </p>
                                <dl class="dl-horizontal pull-left">
                                    <dt>Full Name : </dt>
                                    <dd>
                                        <asp:Label ID="Label1" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>City : </dt>
                                    <dd>
                                        <asp:Label ID="Label2" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Country : </dt>
                                    <dd>
                                        <asp:Label ID="Label3" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Address : </dt>
                                    <dd>
                                        <asp:Label ID="Label4" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Telephone : </dt>
                                    <dd>
                                        <asp:Label ID="Label5" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Mobile : </dt>
                                    <dd>
                                        <asp:Label ID="Label6" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Age : </dt>
                                    <dd>
                                        <asp:Label ID="Label7" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <dt>Gender : </dt>
                                    <dd>
                                        <asp:Label ID="Label8" CssClass="label label-primary" Width="200" runat="server" Text=""></asp:Label></dd>
                                    <br />
                                    <dt>Notes : </dt>
                                    <dd>
                                        <asp:TextBox ID="TextBox1" TextMode="MultiLine"
                                            ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                                    </dd>
                                </dl>

                            </div>
                            <!-- /.panel-body -->
                            

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

