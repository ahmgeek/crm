﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;
using Notify8.Helper;
using System.Threading;

public partial class Cases_Case : System.Web.UI.Page
{
    int id = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString.ToString() != string.Empty)
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
            if (!IsPostBack)
            {
                BindGrid();
                BindRepeater();
            }

        }
        else
        {
            Response.Redirect("/Cases/View.aspx");
        }
    }
    protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void grd_PreRender(object sender, EventArgs e)
    {
        foreach (GridViewRow item in grd.Rows)
        {

            Label lblFullName = (Label)item.FindControl("lblFullName");
            lblFullName.CssClass = "label label-primary";

            Label lblCaseNum = (Label)item.FindControl("lblCaseNum");
            lblCaseNum.CssClass = "label label-primary";

            Label lblCreation = (Label)item.FindControl("lblCreation");
            lblCreation.CssClass = "label label-primary";


            Label lblMob = (Label)item.FindControl("lblMob");
            lblMob.CssClass = "label label-primary";

            Label lblGender = (Label)item.FindControl("lblGender");
            lblGender.CssClass = "label label-primary";
         
            
            Label lblRelation = (Label)item.FindControl("lblRelation");
            if (lblRelation.Text == string.Empty)
            {
                lblRelation.Text = "------------- ------------ -----------";
            }
            lblRelation.CssClass = "label label-default";

        }
    }

    private void BindGrid()
    {
        try
        {
            ClientCase _clientCase = ClientCaseBLL.GetItem(id);

           
          


                //Client Grid

                List<ClientCase> list = new List<ClientCase>();
                list.Add(_clientCase);
                grd.DataSource = list;
                grd.DataBind();

                //CD Grid
                Prescription presc = PrescriptionBLL.GetByCase(id);
                grdCd.DataSource = presc.PrescriptionCds;
                grdCd.DataBind();
                //Course Grid
                grdCourses.DataSource = presc.PrescriptionCourseses;
                grdCourses.DataBind(); ;
                //Sessions Grid
                grdSessions.DataSource = presc.PrescriptionSessions;
                grdSessions.DataBind(); ;
                //Report Bind
                txtFinalReport.Text = presc.Report;
                if (presc.Status == PrescriptionStatus.revised)
                {
                    pnlRevised.Visible = true;
                    txtComment.Text = presc.ConfermedComment;

                }
                //disable editing for ended Cases
                if (presc.Status != PrescriptionStatus.onhold)
                {
                    pnlButtons.Visible = false;
                }
            
        }
        catch (Exception)
        {
            this.ShowNotification("Error", "This Case Is Closed", Notify.NotificationType.error);
            tmr.Enabled = true;
            tmr.Interval = 1500;
            //grd.EmptyDataText = ex.Message;
            //grd.DataBind();
        }

    }

    private void BindRepeater()
    {
        try
        {
            Sessions session = SessionBLL.GetByCase(Convert.ToInt32(Request.QueryString["id"]));
            questionRepeater.DataSource = session.SessionQuestions;
            questionRepeater.DataBind();
            txtReport.Text = session.Report;
        }
        catch (Exception ex)
        {

            grd.EmptyDataText = ex.Message;
            grd.DataBind();
        }

    }

    protected void questionRepeater_PreRender(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in questionRepeater.Items)
        {
            Label answer = (Label) item.FindControl("lblAnswer");
            if (answer.Text == string.Empty)
            {
                answer.Text = "-----";
            };
        }
    }


    protected void btnConfirm_OnClick(object sender, EventArgs e)
    {
        Application["Message"] = "Confirmed";
        Prescription presc = PrescriptionBLL.GetByCase(id);
        presc.Status = PrescriptionStatus.confirmed;
        PrescriptionBLL.Confirm(presc);
        Response.Redirect("/Cases/View.aspx");
        
    }

    protected void btnedit_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("/Cases/CaseEdit.aspx?id="+id);
    }
    protected void tmr_Tick(object sender, EventArgs e)
    {
        Response.Redirect("/Cases/ManageCases.aspx");

    }
}