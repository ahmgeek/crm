﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManageCases.aspx.cs" Inherits="Cases_ManageCases" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <div class="outer">
            <div class="inner">
                
                
                

                <asp:LinkButton ID="btnConfirmed" CssClass="quick-btn" Style="width: 120px;"
                    OnClick="btnConfirmed_Click"
                    runat="server">
           <i class="icon-briefcase icon-2x "></i>
            <span>Confirmed Cases</span>
            <span class="label label-success">Confirmed</span>
                </asp:LinkButton>


                <asp:LinkButton ID="btnOnHold" CssClass="quick-btn" Style="width: 120px;"
                    OnClick="btnOnHold_Click"
                    runat="server">
            <i class="icon-briefcase icon-2x "></i>
            <span>OnHold Cases</span>
            <span class="label label-danger">OnHold</span>
                </asp:LinkButton>


                <asp:LinkButton ID="btnClosed" CssClass="quick-btn" Style="width: 120px;"
                    OnClick="btnClosed_Click"
                    runat="server">
           <i class="icon-briefcase icon-2x "></i>
            <span>Revised Cases</span>
            <span class="label label-warning">Revised</span>
                </asp:LinkButton>




                <asp:LinkButton ID="btnRefresh" CssClass="quick-btn" Style="width: 120px;"
                    OnClick="btnRefresh_Click"
                    runat="server">
           <i class="icon-refresh icon-2x "></i>
            <span>Refresh</span>
            <span class="label label-primary">Refresh</span>
                </asp:LinkButton>





                <div class="row-fluid">
                    <div class="col-lg-9">


                        <div class="box dark">
                            <header>
                                <div class="icons"><i class="icon-briefcase"></i></div>
                                <h5> <span class="label label-default">View Cases</span></h5>
                                <!-- .toolbar -->
                                <div class="toolbar">
                                    <ul class="nav pull-right">
                                        <li>
                                            <asp:TextBox ID="txtSearch" placeholder="Search" runat="server"></asp:TextBox>
                                        </li>
                                        <li>
                                            <asp:Button ID="btnSearch" runat="server" Height="24" CssClass="btn btn-primary btn-sm btn-grad btn-rect" Text="Find" />
                                        </li>
                                        <li>
                                            <a href="#divData" data-toggle="collapse" class="accordion-toggle minimize-box">
                                                <i class="icon-chevron-up"></i>
                                            </a></li>
                                    </ul>

                                </div>
                                <!-- /.toolbar -->
                            </header>
                            <div id="divData" class="accordion-body collapse in body">
                                <div class="form-horizontal">
                                    <div class="table-responsive table-special">
                                        <asp:GridView ID="grd"
                                            CssClass="table table-bordered"
                                            runat="server"
                                            GridLines="None"
                                            CellSpacing="-1"
                                            AutoGenerateColumns="False"
                                            ShowFooter="True" ShowHeaderWhenEmpty="True"
                                            EmptyDataText="Empty !"
                                            OnRowDataBound="grd_RowDataBound"
                                            OnPreRender="grd_PreRender"
                                            OnRowCommand="grd_OnRowCommand"
                                            AllowPaging="true"
                                            PageSize="10">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRank" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <a font-bold="true" style="text-align: left;"
                                                            class="label label-primary"
                                                            href='<%# "/Clients/CLientProfile.aspx?id=" + Eval("ClientId") %>'
                                                            runat="server">
                                                            <asp:Label ID="lblFullName" runat="server" Text='<%#Eval("FullName") %> '></asp:Label>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Case Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCaseNum" runat="server" Text='<%#Eval("CaseNumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Creation Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreation" runat="server" Text='<%#Eval("dateTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Mobile">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMob" runat="server" Text='<%#Eval("Mob") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Gender">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGender" runat="server" Text='<%#Eval("Gender") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("PrescriptionStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEdit"
                                                            CommandArgument='<%# Eval("CaseId") %>' CommandName="ViewCommand"
                                                            runat="server" CssClass="btn btn-primary btn-sm  btn-line" Text="ViewCase" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=btnSearch.ClientID%>').click(function (e) {
                             $("#<%=grd.ClientID%> tr:has(td)").hide(); // Hide all the rows.

                             var iCounter = 0;
                             var sSearchTerm = $('#<%=txtSearch.ClientID%>').val(); //Get the search box value

                      if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
                      {
                          $("#<%=grd.ClientID%> tr:has(td)").show();
                return false;
            }
                             //Iterate through all the td.
                             $("#<%=grd.ClientID%> tr:has(td)").children().each(function () {
                                 var cellText = $(this).text().toLowerCase();
                                 if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
                                 {
                                     $(this).parent().show();
                                     iCounter++;
                                     return true;
                                 }
                             });
                             if (iCounter == 0) {
                             }
                             e.preventDefault();
                         })
                     })
    </script>
</asp:Content>

