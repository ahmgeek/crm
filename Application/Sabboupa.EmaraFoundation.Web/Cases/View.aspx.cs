﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Notify8.Helper;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;

public partial class Cases_View : System.Web.UI.Page
{
    private int rankUser = 0;
    private int currentPageUser = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
            if (Application["Message"]!= null)
            {
                if (Application["Message"]!=string.Empty)
                {
                    this.ShowNotification("Success", Application["Message"].ToString(), Notify.NotificationType.success);
                }
                Application["Message"] = "";
            }
        }
    }


    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblRank = (Label)e.Row.FindControl("lblRank");
        // On page reload, rank is reset to 0
        if (rankUser == 0)
        {
            // Only run this on subsequent pages
            if (currentPageUser > 0)
            {
                // Set rank to current index of page * the number of records to display on GridView page
                rankUser = currentPageUser * grd.PageSize;
            }
        }
        // Make sure we actually found our label
        if (lblRank != null)
        {
            // Increment rank by 1
            rankUser += 1;
            // Set rank label to our new rank value
            lblRank.Text = rankUser.ToString();
        }

    }
    protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        currentPageUser = e.NewPageIndex;
        grd.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void grd_PreRender(object sender, EventArgs e)
    {

        foreach (GridViewRow item in grd.Rows)
        {

            Label lblRank = (Label)item.FindControl("lblRank");

            for (int i = 0; i < rankUser; i++)
            {
                lblRank.CssClass = "badge";

            }


            Label lblFullName = (Label)item.FindControl("lblFullName");
            lblFullName.Width = 200;
            Label lblCaseNum = (Label)item.FindControl("lblCaseNum");
            lblCaseNum.CssClass = "label label-primary";

            Label lblCreation = (Label)item.FindControl("lblCreation");
            lblCreation.CssClass = "label label-primary";


            Label lblMob = (Label)item.FindControl("lblMob");
            lblMob.CssClass = "label label-primary";

            Label lblGender = (Label)item.FindControl("lblGender");
            lblGender.CssClass = "label label-primary";

            Label lblStatus = (Label)item.FindControl("lblStatus");

            if (lblStatus.Text == "confirmed")
            {
                lblStatus.CssClass = "label label-success";

            }
            else if (lblStatus.Text == "onhold")
            {
                lblStatus.CssClass = "label label-danger";
            }
            else if (lblStatus.Text == "revised")
            {
                lblStatus.CssClass = "label label-primary";
            }


        }
    }
    protected void grd_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewCommand")
        {
            Response.Redirect("/Cases/Case.aspx?id=" + e.CommandArgument);
        }
    }

    void BindGrid()
    {
        try
        {
            List<ClientCase> _clientCaseList = ClientCaseBLL.GetListView();
            List<ClientCase> _clientCase = new List<ClientCase>();
            
            //Filtration
            for (int i = 0; i < _clientCaseList.Count; i++)
            {
                if (_clientCaseList[i].PrescriptionStatus == PrescriptionStatus.onhold)
                {
                    _clientCase.Add(_clientCaseList[i]);
                }

            }
            grd.DataSource = _clientCase;
            grd.DataBind();
            

        }
        catch (Exception ex)
        {

            grd.EmptyDataText = ex.Message;
            grd.DataBind();
        }

    }

}