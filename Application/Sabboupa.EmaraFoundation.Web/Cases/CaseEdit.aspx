﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="CaseEdit.aspx.cs" Inherits="Cases_CaseEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .easyWizardSteps {
            list-style: none;
            width: 100%;
            overflow: hidden;
            margin: 0;
            padding: 0;
            border-bottom: 1px solid #ccc;
            margin-bottom: 20px;
        }

            .easyWizardSteps li {
                font-size: 18px;
                display: inline-block;
                padding: 10px;
                color: #B0B1B3;
                margin-right: 20px;
            }

                .easyWizardSteps li span {
                    font-size: 24px;
                }

                .easyWizardSteps li.current {
                    color: #000;
                }

        .easyWizardButtons {
            overflow: hidden;
            padding: 10px;
        }

            .easyWizardButtons button, .easyWizardButtons .submit {
                cursor: pointer;
            }

            .easyWizardButtons .prev {
                float: left;
            }

            .easyWizardButtons .next, .easyWizardButtons .submit {
                float: right;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <div class="outer">
            <div class="inner">

                <div class="box inverse">
                    <header>
                        <div class="icons"><i class="icon-male"></i></div>
                        <h5>Client Data</h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                            <ul class="nav">
                                <li>
                                    <a class="btn btn-link" href="/Cases/View.aspx"><i class="icon-arrow-left"></i>Back
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <!-- /.toolbar -->
                    </header>
                    <div class="table-responsive table-special">
                        <asp:GridView ID="grd"
                            CssClass="table  table-bordered table-condensed table-hover table-striped"
                            runat="server"
                            GridLines="None"
                            CellSpacing="-1"
                            AutoGenerateColumns="False"
                            ShowHeaderWhenEmpty="True"
                            EmptyDataText="Empty !"
                            OnPreRender="grd_PreRender"
                            AllowPaging="true"
                            PageSize="10">
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFullName" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Case Number">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCaseNum" runat="server" Style="width: 100px;" Text='<%#Eval("CaseNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Creation Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreation" runat="server" Style="width: 140px;" Text='<%#Eval("dateTime") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Mobile">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMob" runat="server" Style="width: 90px;" Text='<%#Eval("Mob") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Gender">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGender" runat="server" Style="width: 40px;" Text='<%#Eval("Gender") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Related To">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRelation" runat="server" Style="width: 180px;" Text='<%#Eval("ClientRelName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>
                </div>


                <div id="myWizard" class="form-horizontal">
                    <section class="step" data-step-title="Client Question">

                        <div id="divData" class="accordion-body  collapse in body">
                            <div class="form-horizontal">
                                <div class="box dark">
                                    <header>
                                        <div class="icons"><i class="icon-question"></i></div>
                                        <h5>Questions</h5>
                                        <div class="toolbar">
                                            <ul class="nav">
                                                <li>
                                                    <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#Sessions">
                                                        <i class="icon-chevron-up"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <!-- .toolbar -->
                                        <!-- /.toolbar -->
                                    </header>
                                    <div id="Sessions" style="overflow: scroll; height: 400px;" class="accordion-body collapse in body">
                                        <div class="form-horizontal">
                                            <asp:Repeater ID="questionRepeater" ViewStateMode="Enabled" runat="server" OnPreRender="questionRepeater_PreRender">
                                                <ItemTemplate>
                                                    <div class="form-group">
                                                        <label class="col-lg-1">Question  : </label>
                                                        <div class="col-lg-4" dir="rtl">
                                                            <asp:Label ID="lblQuestion" Style="float: left;" runat="server" CssClass="lbl_ltr" Text='<%#Eval("Question") %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-lg-1">Answer  : </label>
                                                        <div class="col-lg-4" dir="rtl">
                                                            <asp:Label ID="lblAnswer" Style="float: left;" runat="server" CssClass="lbl_ltr label label-primary" Text='<%#Eval("Answer") %>'></asp:Label>
                                                        </div>
                                                    </div>

                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-3">Question Report : </label>

                                            <div class="col-lg-5">

                                                <asp:TextBox runat="server" ID="txtReport" Height="120"
                                                    TextMode="MultiLine" ReadOnly="true" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>


                    </section>
                    <section class="step" data-step-title="The Prescription">

                        <!-- Prescription Edit  -->
                        <div class="box inverse">

                            <header>
                                <div class="icons"><i class="icon-refresh"></i></div>
                                <h5>Revision</h5>
                                <div class="toolbar">
                                    <ul class="nav">
                                        <li>
                                            <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#Presc">
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </header>
                            <div id="Presc" class="accordion-body  collapse in body">
                                <div id="id2" class="form-horizontal">

                                    <div class="form-group">
                                        <label class="col-lg-2">CD's : </label>
                                        <div class="col-lg-4">
                                            <asp:ListBox ID="lstCD"
                                                multiple CssClass="form-control  chzn-select chzn-rtl"
                                                DataValueField="CdDataID"
                                                DataTextField="CdDataName"
                                                SelectionMode="Multiple"
                                                runat="server"></asp:ListBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2">Courses : </label>

                                        <div class="col-lg-4">
                                            <asp:ListBox ID="lstCourses"
                                                SelectionMode="Multiple"
                                                DataValueField="CourseDataID"
                                                DataTextField="CourseDataName"
                                                multiple CssClass="form-control chzn-select  chzn-rtl"
                                                runat="server"></asp:ListBox>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-lg-2">Sessions : </label>
                                        <div class="col-lg-7" style="width: 720px;">
                                            <table class="table responsive">
                                                <tbody>
                                                    <asp:Repeater ID="repeatSessions" ViewStateMode="Enabled" OnPreRender="repeatSessions_OnPreRender" runat="server">
                                                        <ItemTemplate>

                                                            <tr>
                                                                <td>
                                                                    <label>Counter</label><br />
                                                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtCounter" Style="width: 54px;">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <label>Session Name</label><br />
                                                                    <asp:TextBox runat="server" ID="txtSessionName"
                                                                        Text='<%# Eval("SessionDataName")%>' ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <label>Comment</label><br />
                                                                    <asp:TextBox runat="server" ID="txtComment"
                                                                        Width="300" CssClass="form-control">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <label>&nbsp;</label><br />
                                                                    <div class="make-switch">
                                                                        <asp:CheckBox ID="chkCourse" runat="server" />
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                        </div>


                                    </div>

                                    <div class="box inverse">
                                        <header>
                                            <div class="icons"><i class="icon-th-large"></i></div>
                                            <h5>Final Report</h5>
                                            <ul class="nav pull-right">
                                            </ul>
                                        </header>
                                        <br />
                                        <div>
                                            <asp:TextBox ID="txtFinalReport" TextMode="MultiLine" runat="server" CssClass="form-control"
                                                Height="200"></asp:TextBox>
                                        </div>

                                    </div>
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-comment"></i></div>
                                            <h5>Revision Comment</h5>
                                            <ul class="nav pull-right">
                                            </ul>
                                        </header>
                                        <br />
                                        <div>
                                            <asp:TextBox ID="txtRevisionComment" TextMode="MultiLine" CssClass="form-control"
                                                Height="200" runat="server"></asp:TextBox>
                                        </div>
                                        <br />
                                        <div class="form-actions no-margin-bottom">
                                            <asp:Button runat="server" ID="btnUpdate"
                                                CssClass="btn btn-large btn-success" OnClick="btnUpdate_OnClick" Text="Update" />
                                            &nbsp;
                                 <asp:Button runat="server" ID="btnCancel" CausesValidation="False" OnClick="btnCancel_OnClick"
                                     CssClass="btn btn-large btn-warning" Text="Cancel" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

