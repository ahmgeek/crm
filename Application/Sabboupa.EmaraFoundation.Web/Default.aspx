﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div id="content">
        <div class="outer">
            <div class="inner">

<div class="row">
    <!-- .col-lg-6 -->
    <div class="col-lg-6">
        <div class="box inverse" style="height:247px;">
            <header>
                <div class="icons">

    <i class="icon-th-large"></i>

</div>
                <h5>
                    <span class="label label-danger">Quick Links</span>
                </h5>
                <div class="toolbar">
                    <div class="btn-group">		  
                        <a href="#quick" data-toggle="collapse" class="btn btn-default btn-sm accordion-toggle minimize-box">
                            <i class="icon-chevron-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="icon-remove"></i></a>
                    </div>
                </div>
            </header>
                    <div id="quick" class="accordion-body collapse in body">

                        <div class="tac">
                            <a class="quick-btn" href="/Clients/NewClient.aspx">
                                <i class="icon-user icon-2x"></i>
                                <span>New Client </span>
                                <span class="label label-primary">New</span>
                            </a>
                            <a class="quick-btn" href="Clients/Clients.aspx">
                                <i class="icon-male icon-2x"></i>
                                <span>Clients</span>
                                <span class="label label-primary">View</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


      <!-- .col-lg-6 -->
    <div class="col-lg-6">
        <div class="box inverse">
            <header>
                <div class="icons">

    <i class="icon-stackexchange"></i>

</div>
                <h5><span class="label label-success">Statistics</span></h5>
                <div class="toolbar">
                    <div class="btn-group">		  
                        <a href="#condensedTable" data-toggle="collapse" class="btn btn-default btn-sm accordion-toggle minimize-box">
                            <i class="icon-chevron-up"></i>
                        </a>
                        <a class="btn btn-danger btn-sm close-box"><i class="icon-remove"></i></a>
                    </div>
                </div>
            </header>
            <div id="condensedTable" class="accordion-body collapse in body">
                
                <div class="tac">
                    <ul class="stats_box">
                        <li>
                            <div class="sparkline bar_week"></div>
                            <div class="stat_text">
                                <strong>2.345</strong>Total Clients
                <span class="percent down"><i class="icon-caret-up"></i>TODO</span>
                            </div>
                        </li>
                        <li>
                            <div class="sparkline line_day"></div>
                            <div class="stat_text">
                                <strong>165</strong>Daily Visit
                                            <span class="percent up"><i class="icon-caret-up"></i>TODO</span>
                            </div>
                        </li>
                        <li>
                            <div class="sparkline pie_week"></div>
                            <div class="stat_text">
                                <strong>$2 345.00</strong>Weekly Sale
                                            <span class="percent">TODO</span>
                            </div>
                        </li>
                        <li>
                            <div class="sparkline stacked_month"></div>
                            <div class="stat_text">
                                <strong>$678.00</strong>Monthly Sale
                                            <span class="percent down"><i class="icon-caret-down"></i>TODO</span>
                            </div>
                        </li>
                    </ul>
                </div>

                </div>
            </div>
        </div>
    </div>
    </div>
            


        </div>
    </div>

</asp:Content>

