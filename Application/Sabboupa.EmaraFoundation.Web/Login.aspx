﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>System Login</title>

    <link rel="stylesheet" href="/assets/lib/bootstrap/css/bootstrap.css" />

    <link rel="stylesheet" href="/assets/css/login.css" />
    <link rel="stylesheet" href="/assets/lib/magic/magic.css" />
    <link rel="stylesheet" href="/assets/css/main.css" />
    <link rel="stylesheet" href="/assets/lib/Font-Awesome/css/font-awesome.css" />

    <link rel="stylesheet" href="/assets/css/theme.css"/>
    <link rel="stylesheet" href="/assets/lib/prettify/prettify.css"/>
    <link href="assets/Notifiy8/jquery.notific8.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/assets/lib/switch/static/stylesheets/bootstrap-switch.css"/>

</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="text-center">
                <img src="assets/img/main.png" alt="Metis Logo" />
            </div>
                <div id="login" class="tab-pane active">
                    <div class="form-signin">
                        <p class="text-muted text-center">
                            Enter your user name and password
                        </p>
                        <asp:TextBox ID="txtUserName" class="form-control" required placeholder="Username" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtPass" TextMode="Password" required class="form-control" placeholder="Password" runat="server"></asp:TextBox>
                                <label class="label label-primary"> Keep Me Signed In</label>

                            <div class="make-switch switch-small">
                                <asp:CheckBox ID="chkSaveMe"   runat="server" />
                        </div><br /><br />
                        <asp:Button ID="btnLogin" Text="Sign in" Width="300" runat="server" class="btn btn-primary btn-lg btn-grad " OnClick="btnLogin_Click" TabIndex="4" />
                    </div>
                </div>
                <div id="forgot" class="tab-pane">
                </div>


        </div>
        <!-- /container -->
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script src="/assets/lib/jquery-2.0.3.min.js"></script>
        <script src="/assets/lib/bootstrap/js/bootstrap.js"></script>

        <script src="/assets/lib/uniform/jquery.uniform.min.js"></script>
        <script src="/assets/lib/switch/static/js/bootstrap-switch.min.js"></script>
        <script src="/assets/Notifiy8/jquery.notific8.js"></script>

        <script>
            $(function () { formGeneral(); });

        </script>
    </form>
</body>
</html>
