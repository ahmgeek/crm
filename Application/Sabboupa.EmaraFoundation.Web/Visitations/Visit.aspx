﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Visit.aspx.cs" Inherits="Visitations_Visit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="box inverse">
                    <header>
                        <div class="icons"><i class="icon-male"></i></div>
                        <h5>Client Data</h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                            <ul class="nav">
                                <li>
                                    <a class="btn btn-link" href="/Visitations/View.aspx"><i class="icon-arrow-left"></i>Back
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <!-- /.toolbar -->
                    </header>

                    <div class="table-responsive table-special">

                        <asp:GridView ID="grd"
                            CssClass="table  table-bordered table-condensed table-hover table-striped"
                            runat="server"
                            GridLines="None"
                            CellSpacing="-1"
                            AutoGenerateColumns="False"
                            ShowHeaderWhenEmpty="True"
                            EmptyDataText="Empty !"
                            OnPreRender="grd_PreRender"
                            AllowPaging="true"
                            PageSize="10">
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFullName" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Case Number">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCaseNum" runat="server" Style="width: 100px;" Text='<%#Eval("CaseNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Case Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCaseStatus" runat="server" Style="width: 100px;" Text='<%#Eval("PrescriptionStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Creation Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreation" runat="server" Style="width: 140px;" Text='<%#Eval("dateTime") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Mobile">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMob" runat="server" Style="width: 90px;" Text='<%#Eval("Mob") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Gender">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGender" runat="server" Style="width: 40px;" Text='<%#Eval("Gender") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Related To">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRelation" runat="server" Style="width: 180px;" Text='<%#Eval("ClientRelName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

                <asp:Panel ID="pnlAvailable" runat="server">

                    <div class="box inverse">
                        <header>
                            <div class="icons"><i class="icon-briefcase"></i></div>
                            <h5>View Case</h5>
                            <!-- .toolbar -->
                            <div class="toolbar">
                                <ul class="nav">
                                    <li>
                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#divData">
                                            <i class="icon-chevron-down"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.toolbar -->
                        </header>
                        <div id="divData" class="accordion-body  collapse  body">
                            <div class="form-horizontal">

                                <div class="box dark">
                                    <header>
                                        <div class="icons"><i class="icon-question"></i></div>
                                        <h5>Questions</h5>
                                        <div class="toolbar">
                                            <ul class="nav">
                                                <li>
                                                    <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#Sessions">
                                                        <i class="icon-chevron-up"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <!-- .toolbar -->
                                        <!-- /.toolbar -->
                                    </header>
                                    <div id="Sessions" style="overflow: scroll; height: 700px;" class="accordion-body collapse in body">
                                        <asp:Repeater ID="questionRepeater" ViewStateMode="Enabled" runat="server" OnPreRender="questionRepeater_PreRender">
                                            <ItemTemplate>
                                                <div class="form-group">
                                                    <label class="col-lg-1">Question  : </label>
                                                    <div class="col-lg-4" dir="rtl">
                                                        <asp:Label ID="lblQuestion" Style="float: left;" runat="server" CssClass="lbl_ltr" Text='<%#Eval("Question") %>'></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-1">Answer  : </label>
                                                    <div class="col-lg-4" dir="rtl">
                                                        <asp:Label ID="lblAnswer" Style="float: left; font: bold" runat="server" CssClass="lbl_ltr label label-primary" Text='<%#Eval("Answer") %>'></asp:Label>
                                                    </div>
                                                </div>

                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-3">Question Report : </label>

                                            <div class="col-lg-5">

                                                <asp:TextBox runat="server" ID="txtReport" Height="200"
                                                    TextMode="MultiLine" ReadOnly="true" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                </div>




                                <div class="box dark">
                                    <header>
                                        <div class="icons"><i class="icon-medkit"></i></div>
                                        <h5>Prescription</h5>
                                        <div class="toolbar">
                                            <ul class="nav">
                                                <li>
                                                    <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#Prescription">
                                                        <i class="icon-chevron-down"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <!-- .toolbar -->
                                        <!-- /.toolbar -->
                                    </header>
                                    <div id="Prescription" class="accordion-body collapse body">
                                        <div class="form-group" style="overflow: scroll; height: 300px;">
                                            <div class="col-lg-3">
                                                <asp:GridView ID="grdCd" Width="400px"
                                                    CssClass="table  table-bordered table-condensed table-hover table-striped"
                                                    runat="server"
                                                    GridLines="None"
                                                    CellSpacing="-1"
                                                    AutoGenerateColumns="False"
                                                    ShowHeaderWhenEmpty="True"
                                                    EmptyDataText="Empty !"
                                                    OnPreRender="grd_PreRender">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Cd's">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCD" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("CdName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle ForeColor="Maroon" />
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                            <div class="col-lg-3">
                                                <asp:GridView ID="grdCourses" Width="400"
                                                    CssClass="table  table-bordered table-condensed table-hover table-striped"
                                                    runat="server"
                                                    GridLines="None"
                                                    CellSpacing="-1"
                                                    AutoGenerateColumns="False"
                                                    ShowHeaderWhenEmpty="True"
                                                    EmptyDataText="Empty !"
                                                    OnPreRender="grd_PreRender">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Courses">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Courses" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("CourseName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle ForeColor="Maroon" />
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                            <div class="col-lg-3">
                                                <asp:GridView ID="grdSessions" Width="600px"
                                                    CssClass="table  table-bordered table-condensed table-hover table-striped"
                                                    runat="server"
                                                    GridLines="None"
                                                    CellSpacing="-1"
                                                    AutoGenerateColumns="False"
                                                    ShowHeaderWhenEmpty="True"
                                                    EmptyDataText="Empty !"
                                                    OnPreRender="grd_PreRender">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Session Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSessionName" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("SessionName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle ForeColor="Maroon" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="How Many?">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNumber" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("Number") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle ForeColor="#006699" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Comments">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblComment" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("Comment") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle ForeColor="#333333" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <header>
                                        <div class="icons"><i class="icon-medkit"></i></div>
                                        <h5>Final Report</h5>
                                        <div class="toolbar">
                                            <ul class="nav">
                                                <li>
                                                    <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#FinalReport">
                                                        <i class="icon-chevron-down"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <!-- .toolbar -->
                                        <!-- /.toolbar -->
                                    </header>


                                    <div id="FinalReport" class="accordion-body collapse body">
                                        <div class="form-horizontal">

                                            <asp:TextBox ID="txtFinalReport" ReadOnly="True" runat="server" CssClass="form-control"
                                                TextMode="MultiLine" Height="200"></asp:TextBox>
                                            <asp:Panel runat="server" ID="pnlRevised" Visible="false">
                                                <hr />

                                                <div>
                                                    <header>
                                                        <div class="icons"><i class="icon-comment"></i></div>
                                                        <h5>Revision Comment</h5>
                                                        <ul class="nav pull-right">
                                                        </ul>
                                                    </header>
                                                    <br />
                                                    <asp:TextBox ID="txtComment" TextMode="MultiLine" ReadOnly="true" runat="server" CssClass="form-control"
                                                        Height="200"></asp:TextBox>
                                                </div>
                                            </asp:Panel>

                                        </div>
                                    </div>

                                </div>
                                <div class="row-fluid">
                                    <div class="span12">


                                        <div class="box inverse">
                                            <header>
                                                <div class="icons"><i class="icon-phone"></i></div>
                                                <h5>Call Data</h5>
                                                <!-- .toolbar -->
                                                <div class="toolbar">
                                                    <ul class="nav">
                                                        <li>
                                                            <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#divCall">
                                                                <i class="icon-chevron-down"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- /.toolbar -->
                                            </header>
                                            <div id="divCall" class="accordion-body  collapse  body">
                                                <div class="form-horizontal">

                                                    <asp:Panel runat="server"
                                                        ID="pnlAvailble">
                                                        <div class="form-group">
                                                            <label class="col-lg-2">Call Report : </label>
                                                            <div class="col-lg-4">
                                                                <asp:TextBox ID="txtCallReport" Height="200"
                                                                    CssClass="form-control" ReadOnly="true"
                                                                    TextMode="MultiLine"
                                                                    runat="server"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2">Technical Report : </label>
                                                            <div class="col-lg-4">
                                                                <asp:TextBox ID="txtTechnichalReport"
                                                                    Height="200" ReadOnly="true"
                                                                    CssClass="form-control"
                                                                    TextMode="MultiLine"
                                                                    runat="server"></asp:TextBox>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-lg-2">Notes : </label>
                                                            <div class="col-lg-4">
                                                                <asp:TextBox ID="txtNotes" Height="100"
                                                                    CssClass="form-control"
                                                                    TextMode="MultiLine" ReadOnly="true"
                                                                    runat="server"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                    </asp:Panel>





                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </asp:Panel>

            


                <div class="box inverse">
                    <header>
                        <div class="icons"><i class="icon-folder-open"></i></div>
                        <h5>Visit Report</h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                            <ul class="nav">
                                <li>
                                    <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-Visit">
                                        <i class="icon-chevron-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.toolbar -->
                    </header>
                    <div id="div-Visit" class="accordion-body  collapse  body">
                        <div class="form-horizontal">


                            <asp:LinkButton ID="btnVisitDet"
                                class="quick-btn" Style="width: 120px;"
                                OnClick="btnVisitDet_Click"
                                runat="server"> <i class="icon-bookmark icon-2x"></i>
                                   <span>Add New Plan</span>
                                   <span class="label label-info">Treatment</span></asp:LinkButton>


                                <asp:LinkButton ID="btnClose"
                                class="quick-btn" Style="width: 120px;"
                                OnClick="btnClose_Click"
                                    OnClientClick="return confirm('Are you sure you want to clse the case ? ') "
                                runat="server"> <i class="icon-remove-circle icon-2x"></i>
                                   <span>Close the Case</span>
                                   <span class="label label-danger">Close</span></asp:LinkButton>


                            <div class="box">
                                <header>
                                    <h5>Treatment Plans</h5>
                                    <div class="toolbar">
                                        <div class="btn-group">
                                            <a href="#stripedTable" data-toggle="collapse" class="btn btn-default btn-sm accordion-toggle minimize-box">
                                                <i class="icon-chevron-up"></i>
                                            </a>
                                        </div>


                                    </div>
                                </header>
                                    <asp:ScriptManager ID="scrptManager" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="UpdatePanel" runat="server">

                    <ContentTemplate>
                                <div id="stripedTable" class="accordion-body  collapse in body">
                                    <div class="table-responsive table-special">
                                        <br />
                                        <div class="col-lg-3" style="overflow:scroll;height:430px;">
                                            <asp:GridView ID="grdDate"
                                                CssClass="table table-bordered responsive"
                                                runat="server"
                                                GridLines="None"
                                                CellSpacing="-1"
                                                AutoGenerateColumns="False"
                                                ShowFooter="True" ShowHeaderWhenEmpty="True"
                                                EmptyDataText="Empty !"
                                                OnRowDataBound="grdDate_RowDataBound"
                                                OnPreRender="grdDate_PreRender"
                                                OnRowCommand="grdDate_OnRowCommand">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRank" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Visit Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblVisitDate" runat="server" Text='<%# Convert.ToDateTime(Eval("VisitDetTime")).ToString("dd/MM/yyyy") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="View">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEdit"
                                                                CommandArgument='<%# Eval("VisitDetId") %>' CommandName="ViewCommand"
                                                                runat="server" CssClass="btn btn-primary btn-sm  btn-line" Text="Open" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Print">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnPrint"
                                                                CommandArgument='<%# Eval("VisitDetId") %>' CommandName="PrintCommand"
                                                                runat="server" CssClass="btn btn-primary btn-sm  btn-line" Text="Print" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>

                                            </asp:GridView>


                                        </div>
                                        <asp:Panel ID="pnlTreat" runat="server" Visible="false">
                                        <div class="col-lg-9">
                                            <table class="table table-condensed responsive-table">
                                                <thead>
                                                    <tr>
                                                        <th>CD's </th>
                                                        <th>Courses </th>
                                                        <th>Sessions </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>

                                                            <asp:GridView ID="grdVstCD" Width="300px"
                                                                CssClass="table table-bordered responsive"
                                                                runat="server"
                                                                GridLines="None"
                                                                CellSpacing="-1"
                                                                AutoGenerateColumns="False"
                                                                ShowHeaderWhenEmpty="True"
                                                                EmptyDataText="Empty !"
                                                                OnPreRender="grd_PreRender">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Cd's">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCD" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("CdName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle ForeColor="Maroon" />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>

                                                        </td>
                                                        <td>
                                                            <asp:GridView ID="grdVstCourses" Width="300px"
                                                                CssClass="table table-bordered responsive"
                                                                runat="server"
                                                                GridLines="None"
                                                                CellSpacing="-1"
                                                                AutoGenerateColumns="False"
                                                                ShowHeaderWhenEmpty="True"
                                                                EmptyDataText="Empty !"
                                                                OnPreRender="grd_PreRender">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Courses">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Courses" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("Course") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle ForeColor="Maroon" />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>




                                                        </td>
                                                        <td>

                                                            <asp:GridView ID="grdVstSession" Width="300px"
                                                                CssClass="table table-bordered responsive"
                                                                runat="server"
                                                                GridLines="None"
                                                                CellSpacing="-1"
                                                                AutoGenerateColumns="False"
                                                                ShowHeaderWhenEmpty="True"
                                                                EmptyDataText="Empty !"
                                                                OnPreRender="grd_PreRender">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Session Name">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSessionName" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("SessionName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle ForeColor="Maroon" />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>

                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                            <div class="form-group">
                                                <label class="col-lg-1">Period:</label>
                                                <div class="input-group col-lg-4">
                                                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                    <asp:TextBox ID="txtvstPeriod" class="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                    <label class="col-lg-1">Report:</label>

                                            <div class="col-lg-10">
                                            <asp:TextBox ID="txtvstReport" CssClass="form-control"
                                                TextMode="MultiLine" Height="100" ReadOnly="true"
                                                runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                   <label class="col-lg-1">Notes:</label>
                                             <div class="col-lg-10">

                                            <asp:TextBox ID="txtvstNotes" CssClass="form-control"
                                                TextMode="MultiLine" Height="100" ReadOnly="true"
                                                runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                     
                                         
                                        </div>
                                            </asp:Panel>
                                    </div>
                                    
                                </div>
                          </ContentTemplate>
                </asp:UpdatePanel>
                            </div>

                        </div>
                    </div>
                </div>
                        
                  
            </div>
        </div>
    </div>
</asp:Content>

