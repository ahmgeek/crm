﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Notify8.Helper;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;

public partial class Visitaions_View : System.Web.UI.Page
{
    private int rankUser = 0;
    private int currentPageUser = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString.ToString() == string.Empty)
            {
                BindGrid();
            }
            else
            {
                switch (Request.QueryString["visit"])
                {
                    case "Continuous": BindGrid(VisitationStatus.continuation);
                        break;
                    case "All": BindGrid(true);
                        break;

                    default: BindGrid();
                        break;
                }
            }
          
        }
    }


    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblRank = (Label)e.Row.FindControl("lblRank");
        // On page reload, rank is reset to 0
        if (rankUser == 0)
        {
            // Only run this on subsequent pages
            if (currentPageUser > 0)
            {
                // Set rank to current index of page * the number of records to display on GridView page
                rankUser = currentPageUser * grd.PageSize;
            }
        }
        // Make sure we actually found our label
        if (lblRank != null)
        {
            // Increment rank by 1
            rankUser += 1;
            // Set rank label to our new rank value
            lblRank.Text = rankUser.ToString();
        }

    }
    protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        currentPageUser = e.NewPageIndex;
        grd.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void grd_PreRender(object sender, EventArgs e)
    {

        foreach (GridViewRow item in grd.Rows)
        {

            Label lblRank = (Label)item.FindControl("lblRank");

            for (int i = 0; i < rankUser; i++)
            {
                lblRank.CssClass = "badge";

            }


            Label lblFullName = (Label)item.FindControl("lblFullName");

            Label lblCaseNum = (Label)item.FindControl("lblCaseNum");
            lblCaseNum.CssClass = "label label-primary";

            Label lblMob = (Label)item.FindControl("lblMob");
            lblMob.CssClass = "label label-primary";



            Label lblGender = (Label)item.FindControl("lblGender");
            lblGender.CssClass = "label label-primary";

            Label lblFrstCallDate = (Label)item.FindControl("lblFrstCallDate");
            lblFrstCallDate.CssClass = "label label-primary";

            Label lblVisitDate = (Label)item.FindControl("lblVisitDate");
            lblVisitDate.CssClass = "label label-primary";


            Label lblVisitTime = (Label)item.FindControl("lblVisitTime");
            lblVisitTime.CssClass = "label label-primary";




            #region Status
            //Label lblStatus = (Label)item.FindControl("lblStatus");

            //if (lblStatus.Text == "confirmed")
            //{
            //    lblStatus.CssClass = "label label-success";

            //}
            //else if (lblStatus.Text == "onhold")
            //{
            //    lblStatus.CssClass = "label label-danger";
            //}
            //else if (lblStatus.Text == "revised")
            //{
            //    lblStatus.CssClass = "label label-primary";
            //}

            #endregion
        }
    }
    protected void grd_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewCommand")
        {
            Response.Redirect("/Visitations/Visit.aspx?id=" + e.CommandArgument);
        }
    }

    void BindGrid()
    {
        try
        {
            List<ClientCase> _clientCaseList = ClientCaseBLL.GetListView();
            List<ClientCase> _clientCase = new List<ClientCase>();

            //Filtration
            for (int i = 0; i < _clientCaseList.Count; i++)
            {
                if (_clientCaseList[i].FirstCallStatus == FirstCallStatus.available && _clientCaseList[i].VisitDate.Date == DateTime.Now.Date)
                {
                    _clientCase.Add(_clientCaseList[i]);
                }

            }
            grd.DataSource = _clientCase;
            grd.DataBind();


        }
        catch (Exception ex)
        {

            grd.EmptyDataText = ex.Message;
            grd.DataBind();
        }

    }
    void BindGrid(bool all)
    {
        try
        {
            List<ClientCase> _clientCaseList = ClientCaseBLL.GetListView();
            List<ClientCase> _clientCase = new List<ClientCase>();

            //Filtration
            for (int i = 0; i < _clientCaseList.Count; i++)
            {
                if (_clientCaseList[i].FirstCallStatus == FirstCallStatus.available)
                {
                    _clientCase.Add(_clientCaseList[i]);
                }

            }
            grd.DataSource = _clientCase;
            grd.DataBind();


        }
        catch (Exception ex)
        {

            grd.EmptyDataText = ex.Message;
            grd.DataBind();
        }

    }
    void BindGrid(VisitationStatus vst)
    {
        try
        {
            List<ClientCase> _clientCaseList = ClientCaseBLL.GetListView();
            List<ClientCase> _clientCase = new List<ClientCase>();

            //Filtration
            for (int i = 0; i < _clientCaseList.Count; i++)
            {
                if (_clientCaseList[i].FirstCallStatus == FirstCallStatus.available && _clientCaseList[i].VisitationStatus == vst)
                {
                    _clientCase.Add(_clientCaseList[i]);
                }

            }
            grd.DataSource = _clientCase;
            grd.DataBind();


        }
        catch (Exception ex)
        {

            grd.EmptyDataText = ex.Message;
            grd.DataBind();
        }

    }

    protected void btnAll_Click(object sender, EventArgs e)
    {
        Response.Redirect("View.aspx?visit=All");
    }
    protected void btnContin_Click(object sender, EventArgs e)
    {
        Response.Redirect("View.aspx?visit=Continuous");

    }
}