﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VisitDetails.aspx.cs" Inherits="Visitations_VisitDetails" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div id="content">
           <div class="outer">
               <div class="inner">

                               <div class="box inverse">
        <header>
            <div class="icons"><i class="icon-male"></i></div>
            <h5>Client Data</h5>
            <!-- .toolbar -->
            <div class="toolbar">
                <ul class="nav">
                    <li>
                         <a class="btn btn-link" href='<%="/Visitations/Visit.aspx?id="+ Request.QueryString["id"] %>'><i class="icon-arrow-left"></i>Back
                                       </a>
                    </li>
                </ul>
            </div>

            <!-- /.toolbar -->
        </header>
                <div class="table-responsive table-special">
        <asp:GridView ID="grd"
            CssClass="table  table-bordered table-condensed table-hover table-striped"
            runat="server"
            GridLines="None"
            CellSpacing="-1"
            AutoGenerateColumns="False"
            ShowHeaderWhenEmpty="True"
            EmptyDataText="Empty !"
            OnPreRender="grd_PreRender"
            AllowPaging="true"
            PageSize="10">
            <Columns>
                <asp:TemplateField HeaderText="Name">
                    <ItemTemplate>
                        <asp:Label ID="lblFullName" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Case Number">
                    <ItemTemplate>
                        <asp:Label ID="lblCaseNum" runat="server" Style="width: 100px;" Text='<%#Eval("CaseNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Case Status">
                    <ItemTemplate>
                        <asp:Label ID="lblCaseStatus" runat="server" Style="width: 100px;" Text='<%#Eval("PrescriptionStatus") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Creation Date">
                    <ItemTemplate>
                        <asp:Label ID="lblCreation" runat="server" Style="width: 140px;" Text='<%#Eval("dateTime") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Mobile">
                    <ItemTemplate>
                        <asp:Label ID="lblMob" runat="server" Style="width: 90px;" Text='<%#Eval("Mob") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Gender">
                    <ItemTemplate>
                        <asp:Label ID="lblGender" runat="server" Style="width: 40px;" Text='<%#Eval("Gender") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Related To">
                    <ItemTemplate>
                        <asp:Label ID="lblRelation" runat="server" Style="width: 180px;" Text='<%#Eval("ClientRelName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </div>
                </div>



                   <div class="box inverse">
                       <header>
                           <div class="icons"><i class="icon-bookmark-empty"></i></div>
                           <h5>Treatment Plan</h5>
                           <!-- .toolbar -->
                           <div class="toolbar">
                               <ul class="nav">
                                   <li>
                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#divCall">
                                    <i class="icon-chevron-up"></i>
                                </a>
                                   </li>
                               </ul>
                           </div>

                           <!-- /.toolbar -->
                       </header>


                       <div id="divCall" class="accordion-body  collapse in body">
                           <div class="form-horizontal">




                               <div class="form-group">
                                   <label class="col-lg-2">Courses : </label>
                                   <div class="col-lg-4">
                                      <asp:ListBox ID="lstCourses"
                                                    SelectionMode="Multiple"
                                                    DataValueField="CourseDataID"
                                                    DataTextField="CourseDataName"
                                                    multiple CssClass="form-control chzn-select  chzn-rtl"
                                                    runat="server"></asp:ListBox>

                                   </div>
                               </div>
                               
                               <div class="form-group">
                                   <label class="col-lg-2">CD's : </label>
                                   <div class="col-lg-4">
                                        <asp:ListBox ID="lstCD"
                                                    multiple CssClass="form-control  chzn-select chzn-rtl"
                                                    DataValueField="CdDataID"
                                                    DataTextField="CdDataName"
                                                    SelectionMode="Multiple"
                                                    runat="server"></asp:ListBox>

                                   </div>
                               </div>
                               
                               <div class="form-group">
                                   <label class="col-lg-2">Sessions : </label>
                                   <div class="col-lg-4">
                                      <asp:ListBox ID="lstSession"
                                                    SelectionMode="Multiple"
                                                    DataValueField="SessionDataID"
                                                    DataTextField="SessionDataName"
                                                    multiple CssClass="form-control chzn-select  chzn-rtl"
                                                    runat="server"></asp:ListBox>

                                   </div>
                                   
                               </div>
                             
                               <div class="form-group">
                                   <label class="col-lg-2">Period : </label>
                                     <div class="col-lg-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                <asp:TextBox ID="txtPeriod" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                               </div>
                               
                               <div class="form-group">
                                   <label class="col-lg-2">Report : </label>
                                   <div class="col-lg-4">
                                <asp:TextBox ID="txtReport" TextMode="MultiLine"
                                     class="form-control" Height="250" runat="server"></asp:TextBox>


                                   </div>
                               </div>
                                <div class="form-group">
                                   <label class="col-lg-2">Notes : </label>
                                   <div class="col-lg-4">
                                <asp:TextBox ID="txtNotes" TextMode="MultiLine"
                                     class="form-control" Height="190" runat="server"></asp:TextBox>


                                   </div>
                               </div>
                                <div class="form-group">
                                <label class="col-lg-2"> </label>

                                   <div class="col-lg-4">
                                       <asp:Button ID="btnAdd" OnClick="btnAdd_Click"  CssClass="btn btn-large btn-success" 
                                            runat="server" Text="Add Treatment" />

                                       &nbsp; &nbsp; &nbsp;

                                         <asp:Button ID="btnCanel" OnClick="btnCanel_Click"
                                           CssClass="btn btn-large btn-warning"
                                             OnClientClick="return confirm('Are you sure ?')"
                                            runat="server" Text="Canel" />
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
     </div>
</asp:Content>

