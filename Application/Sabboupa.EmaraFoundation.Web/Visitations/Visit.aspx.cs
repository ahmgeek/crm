﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;
using System.Data.SqlTypes;
using Notify8.Helper;
using System.Globalization;
using System.Reflection;
using HTMLReportEngine;
using System.Data;
public partial class Visitations_Visit : System.Web.UI.Page
{
    int id = 0;
    string userName;
    SqlDateTime NullDate;
    private int rankUser = 0;
    private int currentPageUser = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        UsersBLL user = new UsersBLL();

        userName = user.GetUser().UserName;

        if (Request.QueryString.ToString() != string.Empty)
        {

            id = Convert.ToInt32(Request.QueryString["id"]);
            if (Request.QueryString["message"] !=null)
            {
                string mssg = Request.QueryString["message"];
                this.ShowNotification("YaaY", mssg, Notify.NotificationType.success);
            }

            if (!IsPostBack)
            {
                BindGridDate();
                BindGrid();
                BindRepeater();
                BindData();
            }

        }
        else
        {
            Response.Redirect("/Cases/View.aspx");
        }
    }
    protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void grd_PreRender(object sender, EventArgs e)
    {
        foreach (GridViewRow item in grd.Rows)
        {

            Label lblFullName = (Label)item.FindControl("lblFullName");
            lblFullName.CssClass = "label label-primary";

            Label lblCaseNum = (Label)item.FindControl("lblCaseNum");
            lblCaseNum.CssClass = "label label-primary";

            Label lblCreation = (Label)item.FindControl("lblCreation");
            lblCreation.CssClass = "label label-primary";


            Label lblMob = (Label)item.FindControl("lblMob");
            lblMob.CssClass = "label label-primary";

            Label lblGender = (Label)item.FindControl("lblGender");
            lblGender.CssClass = "label label-primary";

            Label lblCaseStatus = (Label)item.FindControl("lblCaseStatus");
            if (lblCaseStatus.Text == "confirmed")
            {
                lblCaseStatus.CssClass = "label label-success";

            }
            else if (lblCaseStatus.Text == "onhold")
            {
                lblCaseStatus.CssClass = "label label-danger";
            }
            else if (lblCaseStatus.Text == "revised")
            {
                lblCaseStatus.CssClass = "label label-default";
            }


            Label lblRelation = (Label)item.FindControl("lblRelation");
            if (lblRelation.Text == string.Empty)
            {
                lblRelation.Text = "------------- ------------ -----------";
            }
            lblRelation.CssClass = "label label-default";

        }
    }

    private void BindGrid()
    {

        try
        {
            //Client Grid
            ClientCase _clientCase = ClientCaseBLL.GetItem(id);
            List<ClientCase> list = new List<ClientCase>();
            list.Add(_clientCase);
            grd.DataSource = list;
            grd.DataBind();

            //CD Grid
            Prescription presc = PrescriptionBLL.GetByCase(id);


            grdCd.DataSource = presc.PrescriptionCds;
            grdCd.DataBind();
            //Course Grid
            grdCourses.DataSource = presc.PrescriptionCourseses;
            grdCourses.DataBind(); ;
            //Sessions Grid
            grdSessions.DataSource = presc.PrescriptionSessions;
            grdSessions.DataBind(); ;
            //Report Bind
            txtFinalReport.Text = presc.Report;
            if (presc.Status == PrescriptionStatus.revised)
            {
                pnlRevised.Visible = true;
                txtComment.Text = presc.ConfermedComment;

            }
        }
        catch (Exception ex)
        {
            this.ShowNotification("Error", "This Case Is Closed", Notify.NotificationType.error);
           // tmr.Enabled = true;
          //  tmr.Interval = 1500;
        }

    }

    private void BindRepeater()
    {
        try
        {
            Sessions session = SessionBLL.GetByCase(Convert.ToInt32(Request.QueryString["id"]));
            questionRepeater.DataSource = session.SessionQuestions;
            questionRepeater.DataBind();
            txtReport.Text = session.Report;
        }
        catch (Exception ex)
        {

            grd.EmptyDataText = ex.Message;
            grd.DataBind();
        }

    }

    protected void questionRepeater_PreRender(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in questionRepeater.Items)
        {
            Label answer = (Label)item.FindControl("lblAnswer");
            if (answer.Text == string.Empty)
            {
                answer.Text = "-----";
            };
        }
    }
    private void BindData()
    {
        FirstCall fCall = FirstCallBLL.GetBtCase(id);
        txtCallReport.Text = fCall.Report;
        txtTechnichalReport.Text = fCall.TechnichalReport;
        txtNotes.Text = fCall.Notes;
    }


    protected void btnVisitDet_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Visitations/VisitDetails.aspx?id="+id);
    }



    #region Treatment

    protected void grdDate_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblRank = (Label)e.Row.FindControl("lblRank");
        // On page reload, rank is reset to 0
        if (rankUser == 0)
        {
            // Only run this on subsequent pages
            if (currentPageUser > 0)
            {
                // Set rank to current index of page * the number of records to display on GridView page
                rankUser = currentPageUser * grd.PageSize;
            }
        }
        // Make sure we actually found our label
        if (lblRank != null)
        {
            // Increment rank by 1
            rankUser += 1;
            // Set rank label to our new rank value
            lblRank.Text = rankUser.ToString();
        }

    }
    protected void grdDate_PreRender(object sender, EventArgs e)
    {

        foreach (GridViewRow item in grdDate.Rows)
        {

            Label lblRank = (Label)item.FindControl("lblRank");

            for (int i = 0; i < rankUser; i++)
            {
                lblRank.CssClass = "badge";

            }

            Label lblVisitDate = (Label)item.FindControl("lblVisitDate");
            lblVisitDate.CssClass = "label label-primary";


        }
    }
    protected void grdDate_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument);
        if (e.CommandName == "ViewCommand")
        {
            pnlTreat.Visible = true;
            BindVisitGrids(id);
            if (Request.QueryString["message"]!=null)
            {
                PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
                // make collection editable
                isreadonly.SetValue(this.Request.QueryString, false, null);
                // remove
                this.Request.QueryString.Remove("message");
            }
        }
        else if (e.CommandName == "PrintCommand")
        {


            // find solution to retrive the dataset from object

            DataSet ds = VisitationDetailBLL.GetDataSet(id, "x");
            DataSet set = new DataSet();
            Report report = new Report();
            report.ReportTitle = "Time Table";
            //ds.Tables["x"];
            
            report.ReportSource = ds;
            //Create Section
            Section release = new Section("Release", "Release: ");

            //Create SubSection
            Section project = new Section("Project", "ProjectID: ");

            //Add the sections to the report
            release.SubSection = project;
            report.Sections.Add(release);

            //Add report fields to the report object.
            report.ReportFields.Add(new Field("TicketNo", "Ticket", 50, ALIGN.RIGHT));
            report.ReportFields.Add(new Field("CreatedBy", "CreatedBy", 150));
            report.ReportFields.Add(new Field("AssignedTo", "AssignedTo"));
            report.ReportFields.Add(new Field("Release", "Release", 200));
            report.ReportFields.Add(new Field("Project", "Project", 150, ALIGN.RIGHT));

            //Generate and save the report
            report.SaveReport(@"d:\Report.htm");
        }
    }

    void BindGridDate()
    {
        try
        {
            List<VisitationDetail> vstDetail = VisitationDetailBLL.GetListByCaseId(id);
            grdDate.DataSource = vstDetail;
            grdDate.DataBind();


        }
        catch (Exception ex)
        {

            grdDate.EmptyDataText = ex.Message;
            grdDate.DataBind();
        }

    }


    private void BindVisitGrids(int vstID)
    {

        try
        {

            //CD Grid
            VisitationDetail vst = VisitationDetailBLL.GetItem(vstID);


            grdVstCD.DataSource = vst.visitationCD;
            grdVstCD.DataBind();
            //Course Grid
            grdVstCourses.DataSource = vst.visitationCourses;
            grdVstCourses.DataBind();
            //Sessions Grid
            grdVstSession.DataSource = vst.visitationSession;
            grdVstSession.DataBind();
            //Report Bind
            txtvstReport.Text = vst.visitDetReport;
            txtvstNotes.Text = vst.notes;
            txtvstPeriod.Text = vst.visitDetPeriod;
        }
        catch (Exception ex)
        {

            
        }

    }

    #endregion



    protected void btnClose_Click(object sender, EventArgs e)
    {
        ClientCase clntCase =  ClientCaseBLL.GetItem(id);
        clntCase.CaseStatus = CaseStatus.closed;
        ClientCaseBLL.UpdateCaseStatus(clntCase);
        Response.Redirect("/Visitations/View.aspx?visit=All");
    }
    //protected void tmr_Tick(object sender, EventArgs e)
    //{
    //    Response.Redirect("/Visitations/View.aspx?visit=All");
    //}
}