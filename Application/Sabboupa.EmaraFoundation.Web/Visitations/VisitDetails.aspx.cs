﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;
using System.Data.SqlTypes;
using Notify8.Helper;
//using System.Transactions;

public partial class Visitations_VisitDetails : System.Web.UI.Page
{
    int id = 0;
    string userName;
    SqlDateTime NullDate;
    protected void Page_Load(object sender, EventArgs e)
    {
        UsersBLL user = new UsersBLL();
        userName = user.GetUser().UserName;
        if (Request.QueryString.ToString() != string.Empty)
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
            if (!IsPostBack)
            {
                BindGrid();
                BindTreatmentData();

            }

        }
        else
        {
            Response.Redirect("/Cases/OnHoldCalls.aspx");
        }
    }
    protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void grd_PreRender(object sender, EventArgs e)
    {
        foreach (GridViewRow item in grd.Rows)
        {

            Label lblFullName = (Label)item.FindControl("lblFullName");
            lblFullName.CssClass = "label label-primary";

            Label lblCaseNum = (Label)item.FindControl("lblCaseNum");
            lblCaseNum.CssClass = "label label-primary";

            Label lblCreation = (Label)item.FindControl("lblCreation");
            lblCreation.CssClass = "label label-primary";


            Label lblMob = (Label)item.FindControl("lblMob");
            lblMob.CssClass = "label label-primary";

            Label lblGender = (Label)item.FindControl("lblGender");
            lblGender.CssClass = "label label-primary";

            Label lblCaseStatus = (Label)item.FindControl("lblCaseStatus");
            if (lblCaseStatus.Text == "confirmed")
            {
                lblCaseStatus.CssClass = "label label-success";

            }
            else if (lblCaseStatus.Text == "onhold")
            {
                lblCaseStatus.CssClass = "label label-danger";
            }
            else if (lblCaseStatus.Text == "revised")
            {
                lblCaseStatus.CssClass = "label label-default";
            }


            Label lblRelation = (Label)item.FindControl("lblRelation");
            if (lblRelation.Text == string.Empty)
            {
                lblRelation.Text = "------------- ------------ -----------";
            }
            lblRelation.CssClass = "label label-default";

        }
    }

    private void BindGrid()
    {

        try
        {
            //Client Grid
            ClientCase _clientCase = ClientCaseBLL.GetItem(id);
            List<ClientCase> list = new List<ClientCase>();
            list.Add(_clientCase);
            grd.DataSource = list;
            grd.DataBind();


        }
        catch (Exception ex)
        {

            grd.EmptyDataText = ex.Message;
            grd.DataBind();
        }



    }


    private void BindTreatmentData()
    {
        try
        {
            List<PrescriptionCourseData> prscCourseData = PrescriptionCourseDataBLL.GetList();
            lstCourses.DataSource = prscCourseData;
            lstCourses.DataBind();

            List<PrescriptionCdData> prscCdData = PrescriptionCdDataBLL.GetList();
            lstCD.DataSource = prscCdData;
            lstCD.DataBind();

            List<PrescriptionSessionData> prscSessionData = PrescriptionSessionDataBLL.GetList();
            lstSession.DataSource = prscSessionData;
            lstSession.DataBind();

        }
        catch (Exception)
        {

            this.ShowNotification("Warning", "Please Fill Course table or CD's table first", Notify.NotificationType.info);
        }
    }

    protected void btnCanel_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
            try
            {
                VisitationDetail vstDetail = new VisitationDetail();
                // Visit Data
                VisitationCourses vstCourses = new VisitationCourses();
                VisitationCD vstCD = new VisitationCD();
                VisitationSession vstSession = new VisitationSession();

             //   using (TransactionScope trans = new TransactionScope())
             //   {
                    // Visit Detail insert
                    vstDetail.caseId = id;
                    vstDetail.visitDetReport = txtReport.Text;
                    vstDetail.visitDetTime = DateTime.Now;
                    vstDetail.visitDetPeriod = txtPeriod.Text;
                    vstDetail.visitStatus = VisitationStatus.continuation;
                    vstDetail.createdBy = userName;
                    vstDetail.notes = txtNotes.Text;
                    vstDetail.visitDetId = VisitationDetailBLL.Insert(vstDetail);

                    // Visit Data insert



                    foreach (ListItem courseItem in lstCourses.Items)
                    {
                        if (courseItem.Selected)
                        {
                            vstCourses.visitDetId = vstDetail.visitDetId;
                            vstCourses.Course = courseItem.Text;
                            vstCourses.Served = HasServed.notDefined;
                            VisitationCoursesBLL.Insert(vstCourses);

                        }
                    }
                    foreach (ListItem cdItem in lstCD.Items)
                    {
                        if (cdItem.Selected)
                        {
                            vstCD.VisitDetId = vstDetail.visitDetId;
                            vstCD.CdName = cdItem.Text;
                            vstCD.Served = HasServed.notDefined;
                            VisitationCdBLL.Insert(vstCD);

                        }
                    }


                    foreach (ListItem sessionItem in lstSession.Items)
                    {
                        if (sessionItem.Selected)
                        {
                            vstSession.VisitDetId = vstDetail.visitDetId;
                            vstSession.SessionName = sessionItem.Text;
                            vstSession.Served = HasServed.notDefined;
                            VisitaionSessionBLL.Insert(vstSession);

                        }
                    }

                   // trans.Complete();
                    //string message = "Treatment plan has been added successfully for the client";
                    Response.Redirect("/Visitations/Visit.aspx?id=" + id,false);//+ "&message=" + message, false);
              //  }
            }
            catch(Exception ex)
            {
                string exip = ex.Message.Replace('\n',' ');
                this.ShowNotification("Error", exip, Notify.NotificationType.error);
            }

       
    }
}