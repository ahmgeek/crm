﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Panda.EmaraSystem.BLL;
using Panda.EmaraSystem.BO;
using System.Data.SqlTypes;
using Notify8.Helper;

public partial class Calls_ViewCall : System.Web.UI.Page
{
    int id = 0;
    string userName;
    SqlDateTime NullDate;
    protected void Page_Load(object sender, EventArgs e)
    {
        UsersBLL user = new UsersBLL();

        userName = user.GetUser().UserName;

        if (Request.QueryString.ToString() != string.Empty)
        {
           


            id = Convert.ToInt32(Request.QueryString["id"]);
            if (!IsPostBack)
            {
                BindGrid();
                BindData();

            }

        }
        else
        {
            Response.Redirect("/Cases/View.aspx");
        }
    }
    protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void grd_PreRender(object sender, EventArgs e)
    {
        foreach (GridViewRow item in grd.Rows)
        {

            Label lblFullName = (Label)item.FindControl("lblFullName");
            lblFullName.CssClass = "label label-primary";

            lblFullName.Width = 200;

            Label lblCaseNum = (Label)item.FindControl("lblCaseNum");
            lblCaseNum.CssClass = "label label-primary";

            Label lblCreation = (Label)item.FindControl("lblCreation");
            lblCreation.CssClass = "label label-primary";


            Label lblMob = (Label)item.FindControl("lblMob");
            lblMob.CssClass = "label label-primary";

            Label lblGender = (Label)item.FindControl("lblGender");
            lblGender.CssClass = "label label-primary";

            Label lblCaseStatus = (Label)item.FindControl("lblCaseStatus");
            if (lblCaseStatus.Text == "confirmed")
            {
                lblCaseStatus.CssClass = "label label-success";

            }
            else if (lblCaseStatus.Text == "onhold")
            {
                lblCaseStatus.CssClass = "label label-danger";
            }
            else if (lblCaseStatus.Text == "revised")
            {
                lblCaseStatus.CssClass = "label label-default";
            }


            Label lblRelation = (Label)item.FindControl("lblRelation");
            if (lblRelation.Text == string.Empty)
            {
                lblRelation.Text = "------------- ------------ -----------";
            }
            lblRelation.CssClass = "label label-default";

        }
    }

    private void BindGrid()
    {

        try
        {
            //Client Grid
            ClientCase _clientCase = ClientCaseBLL.GetItem(id);
            List<ClientCase> list = new List<ClientCase>();
            list.Add(_clientCase);
            grd.DataSource = list;
            grd.DataBind();

            
        }
        catch (Exception ex)
        {
           
            this.ShowNotification("Error", "This Case Is Closed", Notify.NotificationType.error);
            tmr.Enabled = true;
            tmr.Interval = 1500;
          
        }

    }

    private void BindData()
    {
        FirstCall fCall = FirstCallBLL.GetBtCase(id);
        txtCallReport.Text = fCall.Report;
        txtTechnichalReport.Text = fCall.TechnichalReport;
        txtVisitDate.Text = fCall.VisitDate.ToString("dd/MM/yyyy");
        txtTime.Text = fCall.VisitTime;
        txtNotes.Text = fCall.Notes;
    }

    protected void tmr_Tick(object sender, EventArgs e)
    {
        Response.Redirect("/Calls/ConfirmedCalls.aspx");
    }
}