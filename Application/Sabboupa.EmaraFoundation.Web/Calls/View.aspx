﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="View.aspx.cs" Inherits="Calls_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content">
        <div class="outer">
            <div class="inner">
                <a class="quick-btn" style="width: 120px;" href="/Calls/OnHoldCalls.aspx">
                    <i class="icon-phone icon-large "></i>
                    <span>On Hold Calls</span>
                    <span class="label label-danger">On Hold</span>
                </a>


                <asp:LinkButton ID="btnConfirmedCalls" CssClass="quick-btn" Style="width: 120px;"
                    OnClick="btnConfirmedCalls_Click"
                    runat="server">
            <i class="icon-phone-sign icon-large "></i>
            <span>Confirmed Calls</span>
            <span class="label label-success">Confirmed</span>
                </asp:LinkButton>


                <asp:LinkButton ID="btnRefresh" CssClass="quick-btn" Style="width: 120px;"
                    OnClick="btnRefresh_Click"
                    runat="server">
           <i class="icon-refresh icon-large "></i>
            <span>Refresh</span>
            <span class="label label-default">Refresh</span>
                </asp:LinkButton>



                <!-- Cases -->

                <div class="row-fluid">
                    <div class="span12">


                        <div class="box dark">
                            <header>
                                <div class="icons"><i class="icon-phone"></i></div>
                                <h5> <span class="label label-primary">Calls To Activate</span></h5>
                                <!-- .toolbar -->
                                <div class="toolbar">
                                    <ul class="nav pull-right">
                                        <li>
                                            <asp:TextBox ID="txtSearch" placeholder="Search" runat="server"></asp:TextBox>
                                        </li>
                                        <li>
                                            <asp:Button ID="btnSearch" runat="server" Height="24" CssClass="btn btn-primary btn-sm btn-grad btn-rect" Text="Find" />
                                        </li>
                                        <li>
                                            <a href="#divData" data-toggle="collapse" class="accordion-toggle minimize-box">
                                                <i class="icon-chevron-up"></i>
                                            </a></li>


                                    </ul>
                                </div>
                                <!-- /.toolbar -->
                            </header>
                            <div id="divData" class="accordion-body collapse in body">
                                <div class="form-horizontal">
                                    <div class="table-responsive table-special">
                                        <asp:GridView ID="grd"
                                            CssClass="table table-bordered responsive"
                                            runat="server"
                                            GridLines="None"
                                            CellSpacing="-1"
                                            AutoGenerateColumns="False"
                                            ShowFooter="True" ShowHeaderWhenEmpty="True"
                                            EmptyDataText="Empty !"
                                            OnRowDataBound="grd_RowDataBound"
                                            OnPreRender="grd_PreRender"
                                            OnRowCommand="grd_OnRowCommand"
                                            AllowPaging="true"
                                            PageSize="10">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRank" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <a class="label label-primary"
                                                            href='<%# "/Clients/CLientProfile.aspx?id=" + Eval("ClientId") %>'
                                                            runat="server">
                                                            <asp:Label ID="lblFullName" Width="200" Style="text-align: left;" runat="server" Text='<%#Eval("FullName") %> '></asp:Label>
                                                        </a>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Case Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCaseNum" runat="server" Text='<%#Eval("CaseNumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Creation Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreation" runat="server" Text='<%#Eval("dateTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Mobile">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMob" runat="server" Style="text-align: left;" Text='<%#Eval("Mob") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Gender">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGender" runat="server" Text='<%#Eval("Gender") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Case Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("PrescriptionStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEdit"
                                                            CommandArgument='<%# Eval("CaseId") %>' CommandName="EditCommand"
                                                            runat="server" CssClass="btn btn-small btn-danger" Text="Sign First Call" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=btnSearch.ClientID%>').click(function (e) {
                   $("#<%=grd.ClientID%> tr:has(td)").hide(); // Hide all the rows.

                   var iCounter = 0;
                   var sSearchTerm = $('#<%=txtSearch.ClientID%>').val(); //Get the search box value

                      if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
                      {
                          $("#<%=grd.ClientID%> tr:has(td)").show();
                return false;
            }
                   //Iterate through all the td.
                   $("#<%=grd.ClientID%> tr:has(td)").children().each(function () {
                       var cellText = $(this).text().toLowerCase();
                       if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
                       {
                           $(this).parent().show();
                           iCounter++;
                           return true;
                       }
                   });
                   if (iCounter == 0) {
                   }
                   e.preventDefault();
               })
           })
    </script>
</asp:Content>

