﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OnHoldActivation.aspx.cs" Inherits="Calls_OnHoldActivation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div id="content">
        <div class="outer">
            <div class="inner">
            <div class="box inverse">
        <header>
            <div class="icons"><i class="icon-male"></i></div>
            <h5>Client Data</h5>
            <!-- .toolbar -->
            <div class="toolbar">
                <ul class="nav">
                    <li>
                        <a class="btn btn-link" href="/Calls/OnHoldCalls.aspx"><i class="icon-arrow-left"></i>Back
                        </a>
                    </li>
                </ul>
            </div>

            <!-- /.toolbar -->
        </header>
                <div class="table-responsive table-special">
        <asp:GridView ID="grd"
            CssClass="table  table-bordered table-condensed table-hover table-striped"
            runat="server"
            GridLines="None"
            CellSpacing="-1"
            AutoGenerateColumns="False"
            ShowHeaderWhenEmpty="True"
            EmptyDataText="Empty !"
            OnPreRender="grd_PreRender"
            AllowPaging="true"
            PageSize="10">
            <Columns>
                <asp:TemplateField HeaderText="Name">
                    <ItemTemplate>
                        <asp:Label ID="lblFullName" Font-Bold="true" Style="width: 180px;" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Case Number">
                    <ItemTemplate>
                        <asp:Label ID="lblCaseNum" runat="server" Style="width: 100px;" Text='<%#Eval("CaseNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Case Status">
                    <ItemTemplate>
                        <asp:Label ID="lblCaseStatus" runat="server" Style="width: 100px;" Text='<%#Eval("PrescriptionStatus") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Creation Date">
                    <ItemTemplate>
                        <asp:Label ID="lblCreation" runat="server" Style="width: 140px;" Text='<%#Eval("dateTime") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Mobile">
                    <ItemTemplate>
                        <asp:Label ID="lblMob" runat="server" Style="width: 90px;" Text='<%#Eval("Mob") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Gender">
                    <ItemTemplate>
                        <asp:Label ID="lblGender" runat="server" Style="width: 40px;" Text='<%#Eval("Gender") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Related To">
                    <ItemTemplate>
                        <asp:Label ID="lblRelation" runat="server" Style="width: 180px;" Text='<%#Eval("ClientRelName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </div>
                </div>


        <div class="row-fluid">
        <div class="span12">


            <div class="box inverse">
                <header>
                    <div class="icons"><i class="icon-phone"></i></div>
                    <h5>Call Signing</h5>
                    <!-- .toolbar -->
                    <div class="toolbar">
                        <ul class="nav">
                            <li>
                                <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#divCall">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.toolbar -->
                </header>
                <div id="divCall" class="accordion-body  collapse in body">
                    <div class="form-horizontal">

                        <asp:Panel runat="server"
                            ID="pnlAvailble">
                         <div class="form-group">
                            <label class="col-lg-2">Call Report : </label>
                            <div class="col-lg-4">
                                <asp:TextBox ID="txtCallReport" Height="200"
                                    CssClass="form-control" required
                                    TextMode="MultiLine"
                                    runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2">Technical Report : </label>
                            <div class="col-lg-4">
                                <asp:TextBox ID="txtTechnichalReport"
                                    Height="200"
                                    CssClass="form-control"
                                    TextMode="MultiLine"
                                    runat="server"></asp:TextBox>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2">Visit Date : </label>
                            <div class="col-lg-3">
                                 <div class="input-group input-append  date" id="dpYears" data-date="" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                    <asp:TextBox
                                        data-placement="right" ID="txtVisitDate" runat="server"   placeholder="00/00/0000"
                                                        CssClass="form-control" data-date-format="dd/mm/yyyy" required></asp:TextBox>
                                                    <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                            <div class="form-group">
                                <label class="col-lg-2">
                                    Visit Time : </label>
                            <div class="col-lg-3">
                                <div class="input-group bootstrap-timepicker">
                                    <asp:TextBox ID="txtTime" required CssClass="form-control timepicker-default" runat="server">
                                    </asp:TextBox>
                                   <span class="input-group-addon add-on"><i class="icon-time"></i></span>

                                </div>


                            </div>


                        </div>
                               <div class="form-group">
                            <label class="col-lg-2">Notes : </label>
                            <div class="col-lg-4">
                                <asp:TextBox ID="txtNotes" Height="100"
                                    CssClass="form-control"
                                    TextMode="MultiLine"
                                    runat="server"></asp:TextBox>

                            </div>
                        </div>
                            </asp:Panel>

                         <div class="control-group">
                            <div class="controls">

                                <asp:Button ID="btnSave" Width="400"
                                    
                                    OnClick="btnSave_Click"
                                    CssClass="btn btn-large btn-primary"
                                     runat="server" Text="Save" />
                            </div>


                        </div>




                    </div>
                </div>
            </div>
        </div>
        </div>

  </div>
        </div>
        </div>

</asp:Content>

