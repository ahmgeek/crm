﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Panda.EmaraSystem.BLL;
using Notify8.Helper;
using System.Web.Security;

public partial class MasterPage : System.Web.UI.MasterPage
{
    UsersBLL u = new UsersBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        UsersBLL u = new UsersBLL();

        if (Request.IsAuthenticated)
        {
            litName.Text = u.GetUser().UserName.ToUpper();
            litTime.Text = u.GetUser().LastActivityDate.ToString("dd/MM") +" " +
             u.GetUser().LastLoginDate.ToShortTimeString();
            //ltrCount.Text = ClientBLL.GetCount().ToString();
           // ltrUsrsCount.Text = UsersBLL.GetUsersOnline().ToString();

        }
        else
        {
            Response.Redirect("/Login.aspx?ReturnUrl=" + Request.Url.PathAndQuery);
        }
    }


    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        Response.Redirect("/Login.aspx");
    }
    protected void btnSetting_Click(object sender, EventArgs e)
    {
        Response.Redirect("/SystemSettings/Stuff.aspx?name=" + u.GetUser().UserName);
    }

}
