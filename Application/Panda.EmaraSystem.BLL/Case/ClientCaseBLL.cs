﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Panda.EmaraSystem.BO;
using Panda.EmaraSystem.DAL;

namespace Panda.EmaraSystem.BLL
{
    public class ClientCaseBLL
    {

        public static ClientCase GetItem(int id)
        {
            #region Objects
            ClientCase clientCase = ClientCaseDAL.GetItem(id);
            if (clientCase.CaseStatus == CaseStatus.opened)
            {


                Client client = ClientBLL.GetItem(clientCase.ClientId);
                Prescription presc = PrescriptionBLL.GetByCase(clientCase.CaseId);
            #endregion

                clientCase.FullName = client.FullName;
                clientCase.Mob = client.Mob;
                clientCase.Gender = client.Gender;
                clientCase.PrescriptionStatus = presc.Status;
                try
                {
                    Relatives rel = RelativesBLL.GetItem(clientCase.ClientId);
                    clientCase.ClientRelName = rel.ClientRelName;
                    FirstCall fCall = FirstCallBLL.GetBtCase(clientCase.CaseId);
                    clientCase.FirstCallStatus = fCall.Status;
                    VisitationDetail vDet = VisitationDetailBLL.GetByCase(clientCase.CaseId);
                    clientCase.VisitationStatus = vDet.visitStatus;
                    clientCase.dateTime = fCall.dateTime;
                    clientCase.VisitDate = fCall.VisitDate;
                    clientCase.VisitTime = fCall.VisitTime;
                }
                catch (Exception)
                {
                    //Nothing To Handle
                }
            }
            else
            {
                throw new Exception("The case Is closed");
            }
            //Prescription
            
            return clientCase;
        }
        public static ClientCase GetItemClosed(int id)
        {
            #region Objects
            ClientCase clientCase = ClientCaseDAL.GetItem(id);
            if (clientCase.CaseStatus == CaseStatus.closed)
            {


                Client client = ClientBLL.GetItem(clientCase.ClientId);
                Prescription presc = PrescriptionBLL.GetByCase(clientCase.CaseId);
            #endregion

                clientCase.FullName = client.FullName;
                clientCase.Mob = client.Mob;
                clientCase.Gender = client.Gender;
                clientCase.PrescriptionStatus = presc.Status;
                try
                {
                    Relatives rel = RelativesBLL.GetItem(clientCase.ClientId);
                    clientCase.ClientRelName = rel.ClientRelName;
                    FirstCall fCall = FirstCallBLL.GetBtCase(clientCase.CaseId);
                    clientCase.FirstCallStatus = fCall.Status;
                    VisitationDetail vDet = VisitationDetailBLL.GetByCase(clientCase.CaseId);
                    clientCase.VisitationStatus = vDet.visitStatus;
                    clientCase.dateTime = fCall.dateTime;
                    clientCase.VisitDate = fCall.VisitDate;
                    clientCase.VisitTime = fCall.VisitTime;
                }
                catch (Exception)
                {
                    //Nothing To Handle
                }
            }
            else
            {
                throw new Exception("The case Is closed");
            }
            //Prescription

            return clientCase;
        }

        public static List<ClientCase> GetList()
        {
            List<ClientCase> clientCases = new List<ClientCase>();
            List<ClientCase> fltrdClientCases = new List<ClientCase>();
            for (int i = 0; i < clientCases.Count; i++)
            {
                if (clientCases[i].CaseStatus == CaseStatus.opened)
                {
                    fltrdClientCases = clientCases;
                }
              
            }
            return fltrdClientCases;
        }

        public static List<ClientCase> GetListView()
        {
            List<ClientCase> clientCases = ClientCaseDAL.GetList();

            FirstCall fCall = new FirstCall() ;
            for (int i = 0; i < clientCases.Count; i++)
            {
                if (clientCases[i].CaseStatus == CaseStatus.opened)
                {
                    Client client = ClientBLL.GetItem(clientCases[i].ClientId);
                    Prescription prescription = PrescriptionBLL.GetByCase(clientCases[i].CaseId);

                    try
                    {
                        fCall = FirstCallBLL.GetBtCase(clientCases[i].CaseId);
                        clientCases[i].FirstCallStatus = fCall.Status;

                        VisitationDetail vDet = VisitationDetailBLL.GetByCase(clientCases[i].CaseId);
                        clientCases[i].VisitationStatus = vDet.visitStatus;

                    }
                    catch (Exception)
                    {

                    }

                    clientCases[i].FullName = client.FullName;
                    clientCases[i].Mob = client.Mob;
                    clientCases[i].Gender = client.Gender;
                    //Prescription
                    clientCases[i].PrescriptionId = prescription.PrescriptionId;
                    clientCases[i].PrescriptionStatus = prescription.Status;
                    clientCases[i].ConfermedComment = prescription.ConfermedComment;

                    //First Call 
                    clientCases[i].frstCallDateTime = fCall.dateTime;
                    clientCases[i].VisitDate = fCall.VisitDate;
                    clientCases[i].VisitTime = fCall.VisitTime;
                }
               
            }


          
            return clientCases;
        }
        public static List<ClientCase> GetListViewClose()
        {
            List<ClientCase> clientCases = ClientCaseDAL.GetList();

            FirstCall fCall = new FirstCall();
            for (int i = 0; i < clientCases.Count; i++)
            {
                if (clientCases[i].CaseStatus == CaseStatus.closed)
                {
                    Client client = ClientBLL.GetItem(clientCases[i].ClientId);
                    Prescription prescription = PrescriptionBLL.GetByCase(clientCases[i].CaseId);

                    try
                    {
                        fCall = FirstCallBLL.GetBtCase(clientCases[i].CaseId);
                        clientCases[i].FirstCallStatus = fCall.Status;

                        VisitationDetail vDet = VisitationDetailBLL.GetByCase(clientCases[i].CaseId);
                        clientCases[i].VisitationStatus = vDet.visitStatus;

                    }
                    catch (Exception)
                    {

                    }

                    clientCases[i].FullName = client.FullName;
                    clientCases[i].Mob = client.Mob;
                    clientCases[i].Gender = client.Gender;
                    //Prescription
                    clientCases[i].PrescriptionId = prescription.PrescriptionId;
                    clientCases[i].PrescriptionStatus = prescription.Status;
                    clientCases[i].ConfermedComment = prescription.ConfermedComment;

                    //First Call 
                    clientCases[i].frstCallDateTime = fCall.dateTime;
                    clientCases[i].VisitDate = fCall.VisitDate;
                    clientCases[i].VisitTime = fCall.VisitTime;
                }

            }



            return clientCases;
        }

        public static int Insert(ClientCase clientCase)
        {
            clientCase.CaseId = ClientCaseDAL.Insert(clientCase);
            return clientCase.CaseId;
        }

        public static int UpdateByCase(ClientCase clientCase)
        {
            clientCase.CaseId = ClientCaseDAL.Update(clientCase);
            return clientCase.CaseId;
        }

        //Close the case. -> Deactivate all associated data.

        public static int UpdateCaseStatus(ClientCase clientCase)
        {
            clientCase.CaseId = ClientCaseDAL.UpdateCaseStatus(clientCase);
            return clientCase.CaseId;
        }

        //Delete the case. -> Delete all associated data.

    }
}
