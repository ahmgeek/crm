﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Panda.EmaraSystem.BO;
using Panda.EmaraSystem.DAL;

namespace Panda.EmaraSystem.BLL
{
   public class PrescriptionBLL
    {
       public static Prescription GetItem(int id)
       {
           return PrescriptionDAL.GetItem(id);
       }

       //
       public static Prescription GetByCase(int id)
       {
           int prescriptionId=0;
           Prescription presc = PrescriptionDAL.GetByCase(id);

           try
           {
               prescriptionId = presc.PrescriptionId;
               presc.PrescriptionCds = PrescriptionCdDAL.GetListByPrescription(prescriptionId);
               presc.PrescriptionCourseses = PrescriptionCoursesDAL.GetListByPrescription(prescriptionId);
               presc.PrescriptionSessions = PrescriptionSessionDAL.GetListByPrescription(prescriptionId);
           }
           catch (Exception)
           {
               
           }
           return presc;

       }


       public static List<Prescription> GetList()
       {
           return PrescriptionDAL.GetList();
       }

       public static int Insert(Prescription presc)
       {
           presc.PrescriptionId = PrescriptionDAL.Insert(presc);
           return presc.PrescriptionId;
       }

       public static int Update(Prescription presc)
       {
           presc.PrescriptionId = PrescriptionDAL.Update(presc);
           return presc.PrescriptionId;
       }
       //Update By Case
       public static int UpdateByCase(Prescription presc)
       {
           presc.PrescriptionId = PrescriptionDAL.UpdateByCase(presc);
           return presc.PrescriptionId;
       }

       // Update the status only (Confirm)
       public static int Confirm(Prescription presc)
       {
           presc.PrescriptionId = PrescriptionDAL.Confirm(presc);
           return presc.PrescriptionId;
       }


       //public static bool Delete(Prescription presc)
       //{
       //    return PrescriptionDAL.Delete(presc.PrescriptionId);
       //}


    }
}
