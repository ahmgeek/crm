﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Panda.EmaraSystem.BO;
using Panda.EmaraSystem.DAL;

namespace Panda.EmaraSystem.BLL
{
    public class VisitaionSessionBLL
    {



        public static VisitationSession GetItem(int id)
        {
            return VisitationSessionDAL.GetItem(id);
        }
        public static VisitationSession GetByVisitDetId(int id)
        {
            return VisitationSessionDAL.GetByVisitDetId(id);
        }



        public static List<VisitationSession> GetListByVisitDetId(int id)
        {
            List<VisitationSession> visitSession = new List<VisitationSession>();
            try
            {
                visitSession = VisitationSessionDAL.GetListByVisitDet(id);
            }
            catch (Exception)
            {
            }
            return visitSession;
        }


        public static int Insert(VisitationSession VisitationSession)
        {
            VisitationSession.SessionId = VisitationSessionDAL.Insert(VisitationSession);
            return VisitationSession.VisitDetId;
        }
        public static int Update(VisitationSession VisitationSession)
        {
            VisitationSession.VisitDetId = VisitationSessionDAL.Update(VisitationSession);
            return VisitationSession.VisitDetId;
        }


        public static bool Delete(VisitationSession VisitationSession)
        {
            return VisitationSessionDAL.Delete(VisitationSession.SessionId);
        }

    }
}
