﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Panda.EmaraSystem.BO;
using Panda.EmaraSystem.DAL;

namespace Panda.EmaraSystem.BLL
{

    public class VisitationCdBLL
    {
       public static VisitationCD GetItem(int id)
        {
            return VisitationCdDAL.GetItem(id);
        }
       public static VisitationCD GetByVisitDetId(int id)
       {
           return VisitationCdDAL.GetByVisitDetId(id);
       }



       public static List<VisitationCD> GetListByVisitDetId(int id)
       {
           List<VisitationCD> visitCD = new List<VisitationCD>();
           try
           {
               visitCD = VisitationCdDAL.GetListByVisitDet(id);
           }
           catch (Exception)
           {    
           }
           return visitCD;
       }


       public static int Insert(VisitationCD visitCD)
        {
            visitCD.CdId = VisitationCdDAL.Insert(visitCD);
            return visitCD.VisitDetId;
        }
       public static int Update(VisitationCD visitCD)
       {
           visitCD.VisitDetId = VisitationCdDAL.Update(visitCD);
           return visitCD.VisitDetId;
       }


       public static bool Delete(VisitationCD visitCD)
        {
            return PrescriptionCdDAL.Delete(visitCD.CdId);
        }

    }
}
