﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Panda.EmaraSystem.BO;
using Panda.EmaraSystem.DAL;

namespace Panda.EmaraSystem.BLL
{
    public class VisitationDetailBLL
    {

        public static VisitationDetail GetItem(int id)
        {
            int vstDetID = 0;
            VisitationDetail vstDetail = VisitationDetailDAL.GetItem(id);

            try
            {
                vstDetID = vstDetail.visitDetId;
                vstDetail.visitationCD = VisitationCdDAL.GetListByVisitDet(vstDetID);
                vstDetail.visitationCourses = VisitationCoursesDAL.GetListByVisitDet(vstDetID);
                vstDetail.visitationSession = VisitationSessionDAL.GetListByVisitDet(vstDetID);
            }
            catch (Exception)
            {

            }

            return vstDetail;
        }

        public static VisitationDetail GetByCase(int caseId)
        {
            int vstDetID = 0;
            VisitationDetail vstDetail = VisitationDetailDAL.GetByCase(caseId);

            try
            {
                vstDetID = vstDetail.visitDetId;
                vstDetail.visitationCD = VisitationCdDAL.GetListByVisitDet(vstDetID);
                vstDetail.visitationCourses = VisitationCoursesDAL.GetListByVisitDet(vstDetID);
                vstDetail.visitationSession = VisitationSessionDAL.GetListByVisitDet(vstDetID);
            }
            catch (Exception)
            {

            }

            return vstDetail;
        }

        public static List<VisitationDetail> GetListByCaseId(int caseId)
        {
            int visitDetId = 0;
            List<VisitationDetail> visitDetail = VisitationDetailDAL.GetListByCase(caseId);

            try
            {
                for (int i = 0; i < visitDetail.Count; i++)
                {
                    visitDetId = visitDetail[i].visitDetId;
                    visitDetail[i].visitationCourses = VisitationCoursesDAL.GetListByVisitDet(visitDetId);
                    visitDetail[i].visitationSession = VisitationSessionDAL.GetListByVisitDet(visitDetId);
                    visitDetail[i].visitationCD = VisitationCdDAL.GetListByVisitDet(visitDetId);

                }
            }
            catch (Exception)
            {

            }
            return visitDetail;
        }





        public static int Insert(VisitationDetail visitDet)
        {
            visitDet.visitDetId = VisitationDetailDAL.Insert(visitDet);
            return visitDet.visitDetId;
        }
        public static int Update(VisitationDetail visitDet)
        {
            visitDet.visitDetId = VisitationDetailDAL.Update(visitDet);
            return visitDet.visitDetId;
        }
        //Update By Head
        public static int UpdateByHead(VisitationDetail visitDet)
        {
            visitDet.visitDetId = VisitationDetailDAL.UpdateByHead(visitDet);
            return visitDet.visitDetId;
        }

        public static DataSet GetDataSet(int id,string tblName)
        {
            VisitationDetail vst = new VisitationDetail();
            return VisitationDetailDAL.GetDataSet(vst.visitDetId, tblName);
        }

    }
}
