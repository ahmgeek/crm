﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Panda.EmaraSystem.BO;
using Panda.EmaraSystem.DAL;

namespace Panda.EmaraSystem.BLL
{
   public class VisitationCoursesBLL
    {
        public static VisitationCourses GetItem(int id)
        {
            return VisitationCoursesDAL.GetItem(id);
        }
        public static VisitationCourses GetByVisitDetId(int id)
        {
            return VisitationCoursesDAL.GetByVisitDetId(id);
        }



        public static List<VisitationCourses> GetListByVisitDetId(int id)
        {
            List<VisitationCourses> visitCourses = new List<VisitationCourses>();
            try
            {
                visitCourses = VisitationCoursesDAL.GetListByVisitDet(id);
            }
            catch (Exception)
            {
            }
            return visitCourses;
        }


        public static int Insert(VisitationCourses visitCourses)
        {
            visitCourses.CourseId = VisitationCoursesDAL.Insert(visitCourses);
            return visitCourses.visitDetId;
        }
        public static int Update(VisitationCourses visitCourses)
        {
            visitCourses.visitDetId = VisitationCoursesDAL.Update(visitCourses);
            return visitCourses.visitDetId;
        }


        public static bool Delete(VisitationCourses visitCourses)
        {
            return VisitationCoursesDAL.Delete(visitCourses.CourseId);
        }
    }
}
