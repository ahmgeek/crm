﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Panda.EmaraSystem.BO;

namespace Panda.EmaraSystem.DAL
{
    public class VisitationCoursesDAL
    {


        public static VisitationCourses GetItem(int id)
        {
            VisitationCourses visitCourse = null;
            SqlConnection con;
            using (SqlDataReader dr = DataManager.GetDataReader("ESystem_VisitationCourseGetById", out con,
                DataManager.CreateParameter("@CorsesId", SqlDbType.Int, id)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        visitCourse = FillDataRecord(dr);
                    }
                }
                else
                {
                    throw new Exception("No Data");

                }

                con.Close();
            }
            return visitCourse;
        }


        public static VisitationCourses GetByVisitDetId(int id)
        {
            VisitationCourses visitationCourses = null;
            SqlConnection con;
            using (SqlDataReader dr = DataManager.GetDataReader("ESystem_VisitationCourseGetByDet", out con,
                DataManager.CreateParameter("@visitDetailId", SqlDbType.Int, id)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        visitationCourses = FillDataRecord(dr);
                    }
                }
                else
                {
                    throw new Exception("No Data");

                }

                con.Close();
            }
            return visitationCourses;
        }

        public static List<VisitationCourses> GetListByVisitDet(int visitDetId)
        {
            List<VisitationCourses> list = new List<VisitationCourses>();
            SqlConnection con;
            using (SqlDataReader dr =
                DataManager.GetDataReader("ESystem_VisitationCourseGetByDet", out con,
                DataManager.CreateParameter("@visitDetailId", SqlDbType.Int, visitDetId)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        list.Add(FillDataRecord(dr));
                    }
                }
                else
                {
                    throw new Exception("No Data");
                }

                con.Close();
            }
            return list;
        }


        public static int Insert(VisitationCourses visitationCourses)
        {
            object o = DataManager.ExecuteScalar("ESystem_VisitationCoursesInsert",
             DataManager.CreateParameter("@visitDetId", SqlDbType.Int, visitationCourses.visitDetId),
             DataManager.CreateParameter("@course", SqlDbType.NVarChar, visitationCourses.Course),
             DataManager.CreateParameter("@served", SqlDbType.Int, visitationCourses.Served));
            return Convert.ToInt32(o);
        }
        public static int Update(VisitationCourses visitationCourses)
        {
            object o = DataManager.ExecuteScalar("ESystem_VisitationCoursesUpdate",
             DataManager.CreateParameter("@visitDetId", SqlDbType.Int, visitationCourses.visitDetId),
             DataManager.CreateParameter("@course", SqlDbType.NVarChar, visitationCourses.Course),
             DataManager.CreateParameter("@served", SqlDbType.Int, visitationCourses.Served));
            return Convert.ToInt32(o);
        }

        public static bool Delete(int id)
        {
            int result = 0;
            result = DataManager.ExecuteNonQuery("ESystem_VisitationCoursesDelete",
                   DataManager.CreateParameter("@id", SqlDbType.Int, id));

            return result > 0;
        }


        private static VisitationCourses FillDataRecord(IDataRecord myDataRecord)
        {
            VisitationCourses visitCourse = new VisitationCourses();
            visitCourse.CourseId = myDataRecord.GetInt32(myDataRecord.GetOrdinal("CourseId"));
            visitCourse.visitDetId = myDataRecord.GetInt32(myDataRecord.GetOrdinal("VisitDetId"));
            visitCourse.Course = myDataRecord.GetString(myDataRecord.GetOrdinal("Course"));

            if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("Served")))
            {
                visitCourse.Served = (HasServed)myDataRecord.GetInt32(myDataRecord.GetOrdinal("Served"));
            }

            return visitCourse;
        }

    }
}
