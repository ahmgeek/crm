﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Panda.EmaraSystem.BO;
namespace Panda.EmaraSystem.DAL
{
    public class VisitationCdDAL
    {


        public static VisitationCD GetItem(int id)
        {
            VisitationCD visitCd = null;
            SqlConnection con;
            using (SqlDataReader dr = DataManager.GetDataReader("ESystem_VisitationCDGetById", out con,
                DataManager.CreateParameter("@id", SqlDbType.Int, id)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        visitCd = FillDataRecord(dr);
                    }
                }
                else
                {
                    throw new Exception("No Data");

                }

                con.Close();
            }
            return visitCd;
        }



        public static VisitationCD GetByVisitDetId(int id)
        {
            VisitationCD visitationCd = null;
            SqlConnection con;
            using (SqlDataReader dr = DataManager.GetDataReader("ESystem_VisitationCDGetByDet", out con,
                DataManager.CreateParameter("@visitDetailId", SqlDbType.Int, id)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        visitationCd = FillDataRecord(dr);
                    }
                }
                else
                {
                    throw new Exception("No Data");

                }

                con.Close();
            }
            return visitationCd;
        }



        public static List<VisitationCD> GetListByVisitDet(int visitDetId)
        {
            List<VisitationCD> list = new List<VisitationCD>();
            SqlConnection con;
            using (SqlDataReader dr =
                DataManager.GetDataReader("ESystem_VisitationCDGetByDet", out con,
                DataManager.CreateParameter("@visitDetailId", SqlDbType.Int, visitDetId)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        list.Add(FillDataRecord(dr));
                    }
                }
                else
                {
                    throw new Exception("No Data");
                }

                con.Close();
            }
            return list;
        }


        public static int Insert(VisitationCD visitationCD)
        {
            object o = DataManager.ExecuteScalar("ESystem_VisitationCDInsert",
             DataManager.CreateParameter("@visitDetId", SqlDbType.Int, visitationCD.VisitDetId),
             DataManager.CreateParameter("@cdName", SqlDbType.NVarChar, visitationCD.CdName),
             DataManager.CreateParameter("@served", SqlDbType.Int, visitationCD.Served));
            return Convert.ToInt32(o);
        }
        public static int Update(VisitationCD visitationCD)
        {
            object o = DataManager.ExecuteScalar("ESystem_VisitationCDUpdate",
             DataManager.CreateParameter("@visitDetId", SqlDbType.Int, visitationCD.VisitDetId),
             DataManager.CreateParameter("@cdName", SqlDbType.NVarChar, visitationCD.CdName),
             DataManager.CreateParameter("@served", SqlDbType.Int, visitationCD.Served));
            return Convert.ToInt32(o);
        }

        public static bool Delete(int id)
        {
            int result = 0;
            result = DataManager.ExecuteNonQuery("ESystem_VisitationCDDelete",
                   DataManager.CreateParameter("@id", SqlDbType.Int, id));

            return result > 0;
        }


        private static VisitationCD FillDataRecord(IDataRecord myDataRecord)
        {
            VisitationCD visitCD = new VisitationCD();
            visitCD.CdId = myDataRecord.GetInt32(myDataRecord.GetOrdinal("CdId"));
            visitCD.VisitDetId = myDataRecord.GetInt32(myDataRecord.GetOrdinal("VisitDetId"));
            visitCD.CdName = myDataRecord.GetString(myDataRecord.GetOrdinal("CdName"));

            if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("Served")))
            {
                visitCD.Served = (HasServed)myDataRecord.GetInt32(myDataRecord.GetOrdinal("Served"));
            }

            return visitCD;
        }

    }
}
