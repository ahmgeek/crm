﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Panda.EmaraSystem.BO;
using System.Data.SqlClient;

namespace Panda.EmaraSystem.DAL
{
    public class VisitationDetailDAL
    {

        public static VisitationDetail GetItem(int id)
        {
            VisitationDetail visitationDet = null;
            SqlConnection con;
            using (SqlDataReader dr = DataManager.GetDataReader("ESystem_VisitationDetGetById", out con,
                DataManager.CreateParameter("@id", SqlDbType.Int, id)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        visitationDet = FillDataRecord(dr);
                    }
                }
                else
                {
                    throw new Exception("No Data");

                }

                con.Close();
            }
            return visitationDet;
        }

        public static VisitationDetail GetByCase(int headID)
        {
            VisitationDetail visitationDet= null;
            SqlConnection con;
            using (SqlDataReader dr = DataManager.GetDataReader("ESystem_VisitationDetGetByCase", out con,
                DataManager.CreateParameter("@caseId", SqlDbType.Int, headID)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        visitationDet = FillDataRecord(dr);
                    }
                }
                else
                {
                    throw new Exception("No Data");

                }

                con.Close();
            }
            return visitationDet;
        }

        public static List<VisitationDetail> GetListByCase(int caseId)
        {
            List<VisitationDetail> list = new List<VisitationDetail>();
            SqlConnection con;
            using (SqlDataReader dr =
                DataManager.GetDataReader("ESystem_VisitationDetGetAll", out con,
                DataManager.CreateParameter("@id",SqlDbType.Int,caseId)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        list.Add(FillDataRecord(dr));
                    }
                }
                else
                {
                    throw new Exception("No Data");
                }

                con.Close();
            }
            return list;
        }


        public static DataSet GetDataSet(int id,string tblName)
        {
            DataSet ds;
            ds = DataManager.GetDataSet("ESystem_VisitationDetGetById", tblName,
                 DataManager.CreateParameter("@id", SqlDbType.Int, id));
            return ds;
        }


        public static int Insert(VisitationDetail visitDet)
        {
            object o = DataManager.ExecuteScalar("ESystem_VisitationDetailInsert",
             DataManager.CreateParameter("@caseId", SqlDbType.Int, visitDet.caseId),
             DataManager.CreateParameter("@visitDetReport", SqlDbType.NVarChar, visitDet.visitDetReport),
             DataManager.CreateParameter("@visitDetTime", SqlDbType.DateTime, visitDet.visitDetTime),
             DataManager.CreateParameter("@VisitDetPeriod", SqlDbType.NVarChar, visitDet.visitDetPeriod),
             DataManager.CreateParameter("@visitStatus", SqlDbType.Int, visitDet.visitStatus),
             DataManager.CreateParameter("@createdBy", SqlDbType.NVarChar, visitDet.createdBy),
             DataManager.CreateParameter("@notes", SqlDbType.NVarChar, visitDet.notes));

            return Convert.ToInt32(o);
        }
        public static int Update(VisitationDetail visitDet)
        {
            object o = DataManager.ExecuteScalar("ESystem_VisitationDetailUpdate",
             DataManager.CreateParameter("@visitDetId", SqlDbType.Int, visitDet.visitDetId),
             DataManager.CreateParameter("@caseId", SqlDbType.Int, visitDet.caseId),
             DataManager.CreateParameter("@visitDetReport", SqlDbType.NVarChar, visitDet.visitDetReport),
             DataManager.CreateParameter("@visitDetTime", SqlDbType.DateTime, visitDet.visitDetTime),
             DataManager.CreateParameter("@VisitDetPeriod", SqlDbType.NVarChar, visitDet.visitDetPeriod),
             DataManager.CreateParameter("@visitStatus", SqlDbType.NVarChar, visitDet.visitStatus),
             DataManager.CreateParameter("@createdBy", SqlDbType.NVarChar, visitDet.createdBy),
             DataManager.CreateParameter("@notes", SqlDbType.NVarChar, visitDet.notes));

            return Convert.ToInt32(o);
        }
        //Update By Head
        public static int UpdateByHead(VisitationDetail visitDet)
        {
            object o = DataManager.ExecuteScalar("ESystem_VisitationDetailUpdateByHead",
             DataManager.CreateParameter("@caseId", SqlDbType.Int, visitDet.caseId),
             DataManager.CreateParameter("@visitDetReport", SqlDbType.NVarChar, visitDet.visitDetReport),
             DataManager.CreateParameter("@visitDetTime", SqlDbType.DateTime, visitDet.visitDetTime),
             DataManager.CreateParameter("@VisitDetPeriod", SqlDbType.NVarChar, visitDet.visitDetPeriod),
             DataManager.CreateParameter("@visitStatus", SqlDbType.NVarChar, visitDet.visitStatus),
             DataManager.CreateParameter("@createdBy", SqlDbType.NVarChar, visitDet.createdBy),
             DataManager.CreateParameter("@notes", SqlDbType.NVarChar, visitDet.notes));

            return Convert.ToInt32(o);
        }



        private static VisitationDetail FillDataRecord(IDataRecord myDataRecord)
        {
            VisitationDetail visitationDet = new VisitationDetail();

            visitationDet.visitDetId = myDataRecord.GetInt32(myDataRecord.GetOrdinal("VisitDetId"));
            visitationDet.caseId = myDataRecord.GetInt32(myDataRecord.GetOrdinal("CaseId"));

            if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("VisitDetReport")))
            {
                visitationDet.visitDetReport = myDataRecord.GetString(myDataRecord.GetOrdinal("VisitDetReport"));
            }

            if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("VisitDetTime")))
            {
                visitationDet.visitDetTime = myDataRecord.GetDateTime(myDataRecord.GetOrdinal("VisitDetTime"));
            }
            if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("VisitDetPeriod")))
            {
                visitationDet.visitDetPeriod = myDataRecord.GetString(myDataRecord.GetOrdinal("VisitDetPeriod"));
            }

            if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("VisitStatus")))
            {
                visitationDet.visitStatus = (VisitationStatus) myDataRecord.GetInt32(myDataRecord.GetOrdinal("VisitStatus"));
            }
            if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("CreatedBy")))
            {
                visitationDet.createdBy = myDataRecord.GetString(myDataRecord.GetOrdinal("CreatedBy"));
            }
            if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("Notes")))
            {
                visitationDet.notes = myDataRecord.GetString(myDataRecord.GetOrdinal("Notes"));
            }


            return visitationDet;

        }
    }
}
