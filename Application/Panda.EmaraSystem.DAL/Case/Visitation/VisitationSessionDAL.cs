﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Panda.EmaraSystem.BO;

namespace Panda.EmaraSystem.DAL
{
    public class VisitationSessionDAL
    {
        public static VisitationSession GetItem(int id)
        {
            VisitationSession visitationSession = null;
            SqlConnection con;
            using (SqlDataReader dr = DataManager.GetDataReader("ESystem_VisitationSessionGetById", out con,
                DataManager.CreateParameter("@id", SqlDbType.Int, id)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        visitationSession = FillDataRecord(dr);
                    }
                }
                else
                {
                    throw new Exception("No Data");

                }

                con.Close();
            }
            return visitationSession;
        }
        public static VisitationSession GetByVisitDetId(int id)
        {
            VisitationSession visitationSession = null;
            SqlConnection con;
            using (SqlDataReader dr = DataManager.GetDataReader("ESystem_VisitationSessionGetByDet", out con,
                DataManager.CreateParameter("@visitDetId", SqlDbType.Int, id)))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        visitationSession = FillDataRecord(dr);
                    }
                }
                else
                {
                    throw new Exception("No Data");

                }

                con.Close();
            }
            return visitationSession;
        }
        public static List<VisitationSession> GetListByVisitDet(int visitDetId)
       {
           List<VisitationSession> list = new List<VisitationSession>();
           SqlConnection con;
           using (SqlDataReader dr =
               DataManager.GetDataReader("ESystem_VisitationSessionGetByDet", out con,
               DataManager.CreateParameter("@visitDetId", SqlDbType.Int, visitDetId)))
           {
               if (dr.HasRows)
               {
                   while (dr.Read())
                   {
                       list.Add(FillDataRecord(dr));
                   }
               }
               else
               {
                   throw new Exception("No Data");
               }

               con.Close();
           }
           return list;
       }


        public static int Insert(VisitationSession visitationSession)
        {
            object o = DataManager.ExecuteScalar("ESystem_VisitationSessionInsert",
             DataManager.CreateParameter("@visitDetId", SqlDbType.Int, visitationSession.VisitDetId),
             DataManager.CreateParameter("@sessionName", SqlDbType.NVarChar, visitationSession.SessionName),
             //DataManager.CreateParameter("@SessionComment", SqlDbType.Int, visitationSession.SessionComment),
             DataManager.CreateParameter("@served", SqlDbType.Int, visitationSession.Served));
            return Convert.ToInt32(o);
        }
        public static int Update(VisitationSession visitationSession)
        {
            object o = DataManager.ExecuteScalar("ESystem_VisitationSessionUpdate",
             DataManager.CreateParameter("@visitDetId", SqlDbType.Int, visitationSession.VisitDetId),
             DataManager.CreateParameter("@sessionName", SqlDbType.NVarChar, visitationSession.SessionName),
             //DataManager.CreateParameter("@SessionComment", SqlDbType.Int, visitationSession.SessionComment),
             DataManager.CreateParameter("@served", SqlDbType.Int, visitationSession.Served));

            return Convert.ToInt32(o);
        }

        public static bool Delete(int id)
        {
            int result = 0;
            result = DataManager.ExecuteNonQuery("ESystem_VisitationSessionDelete",
                   DataManager.CreateParameter("@id", SqlDbType.Int, id));

            return result > 0;
        }



        private static VisitationSession FillDataRecord(IDataRecord myDataRecord)
        {
            VisitationSession visitationSession = new VisitationSession();

            visitationSession.SessionId = myDataRecord.GetInt32(myDataRecord.GetOrdinal("SessionId"));
            visitationSession.VisitDetId = myDataRecord.GetInt32(myDataRecord.GetOrdinal("VisitDetId"));
            visitationSession.SessionName = myDataRecord.GetString(myDataRecord.GetOrdinal("SessionName"));

            //if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("SessionComment")))
            //{
            //    visitationSession.SessionComment = myDataRecord.GetString(myDataRecord.GetOrdinal("SessionComment"));
            //}

            if (!myDataRecord.IsDBNull(myDataRecord.GetOrdinal("Served")))
            {
                visitationSession.Served = (HasServed)myDataRecord.GetInt32(myDataRecord.GetOrdinal("Served"));
            }


            return visitationSession;
        }

    }
}
