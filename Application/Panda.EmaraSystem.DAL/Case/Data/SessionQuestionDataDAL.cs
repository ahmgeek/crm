﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Panda.EmaraSystem.BO;


namespace Panda.EmaraSystem.DAL
{
    public class SessionQuestionDataDAL
    {
        public static SessionQuestionData GetItem(int id)
        {
            SessionQuestionData sessionData = null;
            SqlConnection con;
            using (SqlDataReader dr = DataManager.GetDataReader("ESystem_SessionDataGetById", out con,
                DataManager.CreateParameter("@sessionDataId", SqlDbType.Int, id)))
            {


                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        sessionData = FillDataRecord(dr);
                    }
                }


                else
                {
                    throw new Exception("No Data");
                }
                con.Close();
            }
            return sessionData;
        }

        public static List<SessionQuestionData> GetList()
        {
            List<SessionQuestionData> list = new List<SessionQuestionData>();
            SqlConnection con;
            using (SqlDataReader dr =
                DataManager.GetDataReader("ESystem_SessionDataGetAll", out con))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        list.Add(FillDataRecord(dr));
                    }
                }
                else
                {
                    throw new Exception("No Data");
                }

                con.Close();
            }
            return list;
        }

        public static int Insert(SessionQuestionData sessionData)
        {
            object o = DataManager.ExecuteScalar("ESystem_SessionDataInsert",
             DataManager.CreateParameter("@questionName", SqlDbType.NVarChar,sessionData.SessionQuestion ));

            return Convert.ToInt32(o);
        }

        public static int Update(SessionQuestionData sessionData)
        {

            object o = DataManager.ExecuteScalar("ESystem_SessionDataUpdate",
             DataManager.CreateParameter("@sessionDataId", SqlDbType.Int, sessionData.SessionDataId),
             DataManager.CreateParameter("@questionName", SqlDbType.NVarChar, sessionData.SessionQuestion));
            return Convert.ToInt32(o);
        }

        public static bool Delete(int id)
        {
            int result = 0;
            result = DataManager.ExecuteNonQuery("ESystem_SessionDataDelete",
                   DataManager.CreateParameter("@sessionDataId", SqlDbType.Int, id));

            return result > 0;
        }


        private static SessionQuestionData FillDataRecord(IDataRecord myDataRecord)
        {
            SessionQuestionData sessionData = new SessionQuestionData();

            sessionData.SessionDataId = myDataRecord.GetInt32(myDataRecord.GetOrdinal("SessionDataID"));

            sessionData.SessionQuestion = myDataRecord.GetString(myDataRecord.GetOrdinal("SessionQuestion"));



            return sessionData;
        }

    }
}
