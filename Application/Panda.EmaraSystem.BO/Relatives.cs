﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Panda.EmaraSystem.BO {
    public class Relatives
    {

        public int RelativeId { get; set; }
        public int ClientId { get; set; }
        public int CLientRelId { get; set; }
        public string ClientName { get; set; }
        public string ClientRelName{ get; set; }
        public string RelationName { get; set; }

    }
}
