﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlTypes;

namespace Panda.EmaraSystem.BO
{
    public class Client
    {
        #region Private Variables
        private List<Relatives> _relatives = new List<Relatives>();


        private IsActive _status = IsActive.NotDefined;


        #endregion

        public int CLientId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SurrName { get; set; }
        public  DateTime CreationDate { get; set; }  
        public string CreatedBy { get; set; }
        public IsActive IsActive
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }
        public string Notes { get; set; }
        public string FullName 
        {
            get
            {
                string tempValue = FirstName;
                if (!String.IsNullOrEmpty(MiddleName))
                {
                    tempValue += " " + MiddleName;
                }
                tempValue += " " + SurrName;
                return tempValue;
            }

        }
        public string City { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Mob { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public HasRelations HasArelation { get; set; }

        public Relatives Relatives { get; set; }
        public List<ClientCase> clientCase { get; set; }

    }
}
