﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Panda.EmaraSystem.BO
{
    public class VisitationSession
    {
        public int SessionId { get; set; }
        public int VisitDetId { get; set; }
        public string SessionName { get; set; }
        //public string SessionComment { get; set; }
        public HasServed Served { get; set; }
    }
}
