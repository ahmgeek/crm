﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Panda.EmaraSystem.BO
{
    public class VisitationCourses
    {
        public int CourseId { get; set; }
        public int visitDetId { get; set; }
        public string Course { get; set; }
        public HasServed Served { get; set; }

    }
}
