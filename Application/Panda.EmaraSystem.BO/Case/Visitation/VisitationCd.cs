﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Panda.EmaraSystem.BO
{
    public class VisitationCD
    {
        public int CdId { get; set; }
        public int VisitDetId { get; set; }
        public string CdName { get; set; }
        public HasServed Served { get; set; }
    }
}
