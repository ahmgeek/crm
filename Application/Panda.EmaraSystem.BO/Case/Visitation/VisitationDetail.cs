﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Panda.EmaraSystem.BO
{
    public class VisitationDetail
    {
        public int visitDetId { get; set; }
        public int caseId { get; set; }
        public string visitDetReport { get; set; }
        public DateTime visitDetTime { get; set; }
        public string visitDetPeriod { get; set; }
        public VisitationStatus visitStatus { get; set; }
        public string createdBy { get; set; }
        public string notes { get; set; }
        

        public List<VisitationCourses> visitationCourses { get; set; }
        public List<VisitationSession> visitationSession{ get; set; }
        public List<VisitationCD> visitationCD{ get; set; }

    }
}
