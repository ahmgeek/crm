﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Panda.EmaraSystem.BO
{
    public class SessionQuestionData
    {
        public int SessionDataId { get; set; }
        public string SessionQuestion { get; set; }
    }
}
