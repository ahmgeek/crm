﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Panda.EmaraSystem.BO
{
    public class ClientCase
    {

        #region PrivateProp

        //private List<Prescription> _prescriptions = new List<Prescription>();
        //private List<Sessions> _sessionse = new List<Sessions>();
        //private List<FirstCall> _firstCall = new List<FirstCall>();
         
        #endregion



        public int CaseId { get; set; }
        public int ClientId { get; set; }
        public string CaseNumber { get; set; }
        public CaseStatus CaseStatus { get; set; }
        public DateTime dateTime { get; set; }

        #region Details Data
        
        //Client Data
        public string FullName { get; set; }
        public string Mob { get; set; }
        public string Gender { get; set; }
        public int ClientRelId { get; set; }
        public string ClientRelName { get; set; }

        //Prescription Data
        public int PrescriptionId { get; set; }
        public PrescriptionStatus PrescriptionStatus { get; set; }
        public string ConfermedComment { get; set; }


        //FirstCall Data
        public DateTime VisitDate { get; set; }
        public string VisitTime { get; set; }
        public DateTime frstCallDateTime { get; set; }
        public FirstCallStatus FirstCallStatus { get; set; }

        //Visitation Data
        public VisitationStatus VisitationStatus { get; set; }


        #endregion



        #region ClientReltions & Dependencies


        //Case Dependencies
        //public Sessions Sessions { get; set; }
        //public Prescription Prescription { get; set; }
        //public FirstCall FirstCall { get; set; }   

        //If the Dependencies more than 1 per Client

        //public List<Prescription> Prescription
        //{
        //    get
        //    {
        //        return _prescriptions;
        //    }
        //    set
        //    {
        //        _prescriptions = value;
        //    }
        //}


        //public List<Sessions> Sessions
        //{
        //    get
        //    {
        //        return _sessionse;
        //    }
        //    set
        //    {
        //        _sessionse = value;
        //    }
        //}

        #endregion
    }
}
